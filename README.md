# Nessys: Nuclear Envelope Segmentation System


## Content

* [What is Nessys?](#what-is-nessys) 
* [Installation?](#installation)
* [How to use Nessys?](#how-to-use-nessys)
* [How can I get help?](#how-can-i-get-help)
* [How is this repository organised?](#how-is-the-repository-organised)
* [How can I contribute?](#how-can-i-contribute)



### What is Nessys?

* It is an extensible software written in Java for the automated identification of cell nuclei in biological images (3D + time). It is designed to perform well in complex samples, i.e when cells are particularly crowded and heterogeneous such as in embryos or in 3D cell cultures. Nessys is also fast and will work on large images which do not fit in memory.

* It offers a generic[^1] interactive user interface for the curation and validation of segmentation results. Think of this as a 3D painter / editor. This editor can also be used to generate manually segmented images to use as ground truth for testing the accuracy of the automated segmentation method.

* It contains a generic[^1] utility for assessing the accuracy of the automated segmentation method. It works by comparing the result of the automated method to a manually generated ground truth. This utility will provide two types of output: a table with a number of metrics about the accuracy and an image representing a map of the mismatch between the result of the automated method and the ground truth. 


A preprint describing Nessys is [available on BiorXiv](https://www.biorxiv.org/content/early/2018/12/20/502872)


[^1]: By 'generic' we mean that the feature will work for any segmented image independently of the tool that generated it.

### Installation

Nessys can be installed as a stand-alone application which should work out of the box. The only requirement is to have [java 8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) or later installed on your computer.

You can download Nessys from [our server](https://datasync.ed.ac.uk/index.php/s/MaJm88G08PO5euE) (password is 'nessys'). To install and run Nessys, please follow the instructions in the Readme.txt file within the zip archive (The installation process is very easy, basically unzipping and running a small script).


### How to use Nessys?

A tutorial is available online at https://pickcellslab.frama.io/docs/use/features/segmentation/nessys/

You will notice that Nessys also comes as part of PickCells. More information on the PickCells software can be found on the [website](https://pickcellslab.frama.io/docs/)


### How can I get help?

You are very welcome to look for help on [our forum](https://colony.pickcellslab.org/). Search for the tag 'nessys' to look for posts related to this software and register if you would like to ask your own questions.


### How is the repository organised?

Nessys is managed using [Maven](https://maven.apache.org/). 

In this repository, you will find 3 projects:

* nessys-core : This project contains the core classes of Nessys which handle the logic for segmentation, editing and measuring segmentation accuracy. Note that Nessys depends on an abstract layer which is part of the PickCell's API. This layer of abstraction allows for nessys to be plugged into various types of frameworks Please see the nessys-core subproject for more details.
* nessys-standalone : This project contains an aggregator pom to build the standalone nessys application. The project only contains one class 'NessysLauncher' which possesses the main method and builds the initial dialog to load the desired Nessys feature. Note that the standalone assembly uses PickCells implementations and other third parties libraries, notably for image input/output and image display. Please see the 'nessys-standalone' subproject for more details.
* nessys-for-pickcells: This project contains adaptor classes to plug nessys into the PickCells application.

NB: We do also have plans to create a subproject to port Nessys as a [FIJI](https://fiji.sc/) plugin.


### How can I contribute?

1. By providing feedback which is very welcome. To do so, please use [our forum](https://colony.pickcellslab.org/) and add the tag 'nessys' to your posts
2. By filing an issue on the [issue tracker](https://framagit.org/pickcellslab/nessys/issues)
3. By forking this repository and [creating a merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).