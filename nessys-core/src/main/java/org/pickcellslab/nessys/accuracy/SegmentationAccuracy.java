package org.pickcellslab.nessys.accuracy;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.tools.MutableInt;
import org.pickcellslab.nessys.accuracy.ComparisonBuilder.Comparison;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.Nucleus;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.io.ImgsChooser;
import org.pickcellslab.pickcells.api.img.io.ImgsChooserBuilder;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryUtils;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;

import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;

import net.imglib2.FinalInterval;
import net.imglib2.Interval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.localneighborhood.Utils;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.view.Views;


public class SegmentationAccuracy<R extends NativeType<R> & RealType<R>, T extends NativeType<T> & RealType<T>>{


	private final AKey<Integer> caseKey = AKey.get("Case", Integer.class);
	private static final int EXCLUDED = 2, MISS = 3, SPURIOUS = 4, SPLIT = 5, MERGED = 6;

	private final float miss_spur = 0.95f; // The percentage of the volume of a shape with background label for counting as missed or spurious
	private static String dir;

	
	
	private final List<SegmentationComparator> fullImageComps = new ArrayList<>();
	private final List<SegmentationComparator> matchingComps = new ArrayList<>();
	
	private final ProviderFactory pf;
	private final ImgIO io;
	
	
	
	
	public SegmentationAccuracy(ProviderFactoryFactory pff, ImgIO io) {
		fullImageComps.add(new ClusteringComparator());
		matchingComps.add(new DistanceBasedComparator());
		this.pf = pff.create(1);
		this.io = io;
	}


	
	
	public void run() throws Exception {

		Map<String, List<Comparison>> results = new LinkedHashMap<>();


		// Ref Images
		ImgFileList refList = getReferences("Choose the reference image(s)");
		if(refList == null)
			return;

		// Images to test
		ImgFileList testList = getTestList(refList);
		if(testList == null)
			return;


		ImgsChecker refCheck = io.createCheckerBuilder().build(refList);
		ImgsChecker testCheck = io.createCheckerBuilder().build(testList);

		// For each image now perform the comparison
		for(int i = 0; i < refList.numDataSets(); i++){
			for(int j = 0; j < refList.numImages(i); j++){

				final List<Comparison> comparisons = new ArrayList<>();



				// Obtain the list of matchings
				// for this we need providers
				final Image refImage = ProviderFactoryUtils.createImage(io, refCheck, refList, i, j);
				final Image testImage = ProviderFactoryUtils.createImage(io, testCheck, testList, i, j);
				final LabelsImage refLabels = ProviderFactoryUtils.createLabels(refImage, refCheck, refList, i, j);
				final LabelsImage testLabels = ProviderFactoryUtils.createLabels(testImage, testCheck, testList, i, j);




				final Img<R> ref = refList.load(i, j);
				final Img<T> test = testList.load(i, j);

				final Img<UnsignedByteType> out = io.createImg(ref, new UnsignedByteType());


				fullImageComps.forEach(comp->{
					comp.addTest(io, ref, test);
					comparisons.add(comp.getResult());
					comp.reset();
				});

				final SegmentationImageProvider<R> refProv = pf.createFrom(refLabels, ref);
				final SegmentationImageProvider<T> testProv = pf.createFrom(testLabels, test);
				refProv.setPrototype(refLabels, new Nucleus(), false);
				testProv.setPrototype(testLabels, new Nucleus(), false);

				// TODO for every time points	



				// Fwd Matching : Count miss when 95% of voxels are bg
				// Count Merge when a color in test is found as best match for more than 1 color in ref

				final Map<Float, MutableInt> fwdMap = new HashMap<>();
				final MutableInt miss = new MutableInt();
				final MutableInt refBorder = new MutableInt();

				final BiFunction<Float,MatchFinder<T>,Boolean> missCheck = (refLabel,finder) ->{

					final SegmentedObject refObject = refProv.getItem(refLabel,0).get();
					final float bestMatchInTest = finder.getBestMatch();

					if(refObject.getAttribute(Keys.isborder).get()){// Exclude if border cell
						refBorder.increment();
						refObject.setAttribute(caseKey, EXCLUDED);
						return false;
					}
					else if(bestMatchInTest == 0f){
						miss.increment();
						refObject.setAttribute(caseKey, MISS);
						System.out.println("Missed : "+refLabel);
						return false;
					}
					else{
						MutableInt count = fwdMap.get(bestMatchInTest);
						if(null==count)
							fwdMap.put(bestMatchInTest, new MutableInt(1));
						else{
							System.out.println("Merged : "+refLabel);
							testProv.getItem(bestMatchInTest,0).get().setAttribute(caseKey, MERGED);
							count.increment();
						}
						return true;
					}					
				};

				final BiConsumer<RandomAccessibleInterval<R>, RandomAccessibleInterval<T>> consumer = (refBinary,testBinary) ->{					
					matchingComps.forEach(c->c.addTest(io, refBinary, testBinary));					
				};

				// count as miss if > 95 % of voxels are background.
				this.processMatchings(refProv, testProv, missCheck, consumer, miss_spur);


				matchingComps.forEach(c->{
					comparisons.add(c.getResult());
					c.reset();
				});



				// Bckwd Matching ----------------------------------------------

				final Map<Float, MutableInt> bckMap = new HashMap<>();
				final MutableInt adds = new MutableInt();
				final MutableInt stretch = new MutableInt();
				final MutableInt testBorder = new MutableInt();

				final BiFunction<Float,MatchFinder<R>,Boolean> addCheck = (testLabel,finder) ->{
					if(finder.isStretch()){
						System.out.println("Stretched (testLabel): "+testLabel);
						stretch.increment();
					}

					final float bestMatchInRef = finder.getBestMatch();
					final SegmentedObject testObject = testProv.getItem(testLabel,0).get();

					if(bestMatchInRef == 0f){
						System.out.println("Spurious (test label) : "+testLabel);
						testObject.setAttribute(caseKey, SPURIOUS);
						adds.increment();
						return false;
					}
					else if(refProv.getItem(bestMatchInRef,0).get().getAttribute(Keys.isborder).get()){// Exclude if best match (in ref) is border
						testBorder.increment();
						return false;
					}
					else{
						MutableInt count = bckMap.get(bestMatchInRef);
						if(null==count)
							bckMap.put(bestMatchInRef, new MutableInt(1));
						else{
							System.out.println("Split : "+bestMatchInRef);
							refProv.getItem(bestMatchInRef,0).get().setAttribute(caseKey, SPLIT);
							count.increment();
						}
						return false;
					}					
				};


				// Count as Spurious if > 95 % of voxels are background
				this.processMatchings(testProv, refProv, addCheck, null, miss_spur);


				ComparisonBuilder b = new ComparisonBuilder(getClass());

				b.setValue("Miss", miss.intValue());
				b.setValue("Spurious", adds.intValue());
				b.setValue("Stretch", stretch.intValue());

				SummaryStatistics stats1 = new SummaryStatistics();
				for( MutableInt e : fwdMap.values())
					if(e.intValue()>1)
						stats1.addValue(e.doubleValue());

				b.setValue("Merge count", stats1.getN());
				b.setValue("Merge size Mean", stats1.getMean());
				b.setValue("Merge size Std", stats1.getStandardDeviation());

				stats1.clear();
				for( MutableInt e : bckMap.values())
					if(e.intValue()>1)
						stats1.addValue(e.doubleValue());

				b.setValue("Split count", stats1.getN());
				b.setValue("Split size Mean", stats1.getMean());
				b.setValue("Split size Std", stats1.getStandardDeviation());

				b.setValue("Ref Total Number of Cells ", refProv.labels(0).count());
				b.setValue("Ref Number of Border Cells ", refBorder.intValue());
				b.setValue("Tested Total Number of Cells ", testProv.labels(0).count());
				b.setValue("Test Number of Border Cells ", testBorder.intValue());

				comparisons.add(b.build());

				results.put(testList.name(i,j), comparisons);

				// Create output image
				final UnsignedByteType color = new UnsignedByteType();
				final RandomAccess<UnsignedByteType> ra = out.randomAccess();
				final Consumer<long[]> outline = l->{
					ra.setPosition(l);
					ra.get().set(color);
				};

				refProv.labels(0).sequential().forEach(refLabel->{
					float col = refProv.getItem(refLabel, 0).get().getAttribute(caseKey).orElse(1);
					//if(col!=0){
						color.setReal(col);
						refProv.processOutline(refLabel, 0, outline);						
					//}
				});
				testProv.labels(0).sequential().forEach(testLabel->{
					float col = testProv.getItem(testLabel, 0).get().getAttribute(caseKey).orElse(0);
					if(col!=0){
						color.setReal(col);
						testProv.processOutline(testLabel, 0, outline);						
					}
				});

				// Save output Image
				io.save(testImage.getMinimalInfo(), out, testList.path(0)+File.separator+"Comparison_"+testList.name(i,j));

			}
		}



		saveResult(results, refList.path(0));

		WebNotification wn = NotificationManager.showNotification("Accuracy Test Done!");
		wn.setDisplayTime(1000);

	}





	private <F extends NativeType<F> & RealType<F>, S extends NativeType<S> & RealType<S>> void processMatchings(SegmentationImageProvider<F> refProv, SegmentationImageProvider<S> testProv,
			BiFunction<Float,MatchFinder<S>,Boolean> block,
			BiConsumer<RandomAccessibleInterval<F>, RandomAccessibleInterval<S>> consumer,
			float zeroThresh){


		
		final ImgFactory<F> refFctry = new ArrayImgFactory<>();
		final F refType = refProv.variable(0);
		final ImgFactory<S> testFctry = new ArrayImgFactory<>();
		final S testType = testProv.variable(0);

		final RandomAccessibleInterval<F> refFrame = refProv.getFrame(0);
		final RandomAccessibleInterval<S> testFrame = testProv.getFrame(0);

		final RandomAccess<S> testA = testFrame.randomAccess();
		final long[] matchMin = new long[testA.numDimensions()];
		final long[] matchMax = new long[testA.numDimensions()];



		refProv.labels(0).sequential().forEach(refLabel->{


			final MatchFinder<S> finder = new MatchFinder<>(testA, zeroThresh);
			refProv.processPoints(refLabel, 0, finder);

			if(block.apply(refLabel, finder)) {


				// Create the union interval and submit to comparators
				SegmentedObject refObj = refProv.getItem(refLabel, 0).get();
				SegmentedObject testObj = testProv.getItem(finder.getBestMatch(), 0).get();


				long[] refMin = refObj.getAttribute(Keys.bbMin).get();
				long[] refMax = refObj.getAttribute(Keys.bbMax).get();
				long[] testMin = testObj.getAttribute(Keys.bbMin).get();
				long[] testMax = testObj.getAttribute(Keys.bbMax).get();

				for(int d = 0; d<matchMin.length; d++){
					matchMin[d] = Math.min(refMin[d], testMin[d]);
					matchMax[d] = Math.max(refMax[d], testMax[d]);
				}

				Interval itrv = new FinalInterval(matchMin, matchMax);
				
				//System.out.println("Interval: min = "+Arrays.toString(matchMin) + " ; max = "+Arrays.toString(matchMax));
				
				Img<F> refBinary = refFctry.create(itrv, refType);
				Img<S> testBinary = testFctry.create(itrv, testType);

				ImgDimensions.copy(Views.offsetInterval(refFrame, itrv), refBinary);
				ImgDimensions.copy(Views.offsetInterval(testFrame, itrv), testBinary);

				consumer.accept(refBinary, testBinary);

			}
		});



	}







	private ImgFileList getReferences(String title) {

		ImgsChooserBuilder builder = io.createChooserBuilder().setTitle(title);
		if(dir!=null)
			builder.setHomeDirectory(dir);
		ImgsChooser chooser = builder.build();

		chooser.setModal(true);
		chooser.setVisible(true);

		if(chooser.wasCancelled())	return null;

		ImgFileList refList = chooser.getChosenList();

		dir = refList.path(0);

		if(refList.numImages()==0){
			JOptionPane.showMessageDialog(null, "The dataset is empty!");
			return getReferences(title);
		}


		for(int i = 0; i < refList.numDataSets(); i++){
			for(int j = 0; j < refList.numImages(i); j++){

				System.out.println("Checking Image data : "+refList.name(i, j));

				//Check that the segmentation has only one channel
				//and that the other dimensions are the same as the color image 
				final int k = j;
				ImgsChecker check = 
						io.createCheckerBuilder()	
						.addCheck(ImgsChecker.nChannels,(Integer)1)
						//.addCheck(ImgsChecker.dimensions, colorDims)
						.build(refList.subList(i, p -> p==k ));

				if(!check.isConsistent()){
					JOptionPane.showMessageDialog(null, "Segmentation Images must have only one channel");
					return getReferences(title);
				}
			}
		}




		return refList;
	}






	private ImgFileList getTestList(ImgFileList refList) {


		ImgFileList testList = getReferences("Choose the image(s) to be compared");
		if(testList == null)
			return null;

		if(refList.numImages()!=testList.numImages()){
			JOptionPane.showMessageDialog(null, "You need to import the same number of reference images and images to be tested");
			return getTestList(refList);
		}



		int counter = 0;
		for(int i = 0; i < refList.numDataSets(); i++){
			for(int j = 0; j < refList.numImages(i); j++){

				//Get the raw image dimensions


				System.out.println("Checking Consistency : "+refList.name(i, j)+" and "+ testList.name(i, j));

				//Check that the segmentation has only one channel
				//and that the other dimensions are the same as the color image 

				final int k = j;
				ImgsChecker check = 
						io.createCheckerBuilder()
						.build(refList.subList(i, p -> p==k ));

				long[] refDims = check.value(ImgsChecker.dimensions, refList,i,j);
				long[] testDims = check.value(ImgsChecker.dimensions, testList,i,j);


				//Check that the segmentation image has the same dimensions as the original image
				if(!Arrays.equals(refDims,testDims)){
					JOptionPane.showMessageDialog(null, 
							"<HTML>The segmentation result selected for image "+counter+" does"
									+ "<br> not have the same dimensions as the parent image :"
									+ "<br><b>Ref dimensions:</b> "+Arrays.toString(refDims)
									+ "<br><b>Test dimensions :</b> "+Arrays.toString(testDims)+"/HTML");
					return null;
				}
			}
			counter++;
		}

		return testList;
	}






	private void saveResult(Map<String, List<Comparison>> results, String path) throws FileNotFoundException{



		PrintWriter os = new PrintWriter(path+File.separator+new Date().toString()+"_ComparisonResults.txt");

		// Print Column Headers
		for(String imgName : results.keySet()){
			os.print("\t"+imgName);
		}
		os.println();

		// All lists are the same size
		List<Comparison> first = results.values().iterator().next();
		int size = first.size();
		for(int i = 0; i<size; i++){

			// Print Creator
			os.println(first.get(i).creator()+"\t");

			for(String measure : first.get(i).computedMeasures(Modus.BASIC)){

				os.print(measure+"\t");

				for(String imgName : results.keySet()){
					os.print(results.get(imgName).get(i).getValueFor(measure)+"\t");
				}

				os.println();
			}
		}




		os.flush();
		os.close();
	}








	
	public String name() {
		return "Segmentation Metric";
	}

	
	public String description() {
		return "<HTML>Determines the accuracy of a segmentation algorithm by comparing a result"
				+ "<br> with the manually corrected image. Measures are based on Coelho et al IEEEXplore 2009</HTML>";
	}






	
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/accuracy_icon.png"));
	}






	/**
	 * Record the labels that are found at given positions in an image
	 * and counts the number of time each label has been encountered.
	 * <br>
	 * Used here to identify a matching volume in a test image
	 *
	 * @param <I> The type of the image where the match needs to be found
	 *
	 */
	private class MatchFinder<I extends NativeType<I> & RealType<I>> implements Consumer<long[]>{

		private final RandomAccess<I> ra;
		private final Map<Float, Integer> counter = new HashMap<>();

		private final float zeroThresh;

		private float r = -1;

		private int total = 0;


		/**
		 * @param ra The {@link RandomAccess} on the image where labels are to be recorded
		 * @param zeroThresh The value of the background
		 */
		public MatchFinder(RandomAccess<I> ra, float zeroThresh) {
			this.ra = ra;
			this.zeroThresh = zeroThresh;
		}


		@Override
		public void accept(long[] t) {
			ra.setPosition(t);
			Float f = ra.get().getRealFloat();
			Integer i = counter.get(f);
			if(null == i)
				counter.put(f, 1);
			else
				counter.put(f, counter.get(f)+1);
			total++;
		}


		/**
		 * @return The label in the other image which has the largest overlap with the currently tested object or
		 * 0 (bg) if the amount of overlap with the background is larger zeroThresh
		 */
		public float getBestMatch(){
			// Check background
			Integer bg = counter.get(0f);
			if(bg != null && bg > zeroThresh*(float)total)
				return 0f;

			int max = -1;
			if(r==-1)
				for(Entry<Float, Integer> entry : counter.entrySet()){
					if(entry.getKey()!= 0f && entry.getValue()>max){
						max = entry.getValue();
						r = entry.getKey();
					}
				}
			return r;
		}


		/**
		 * @return true if the positions visited by this {@link MatchFinder} span
		 * at least 2 distinct labels, each of which cover at least 25% of the total volume 
		 */
		public boolean isStretch(){

			double thresh = 0.25d * (double)total;

			int count = 0;
			for(Entry<Float, Integer> entry : counter.entrySet())
				if(entry.getKey()!= 0f && entry.getValue() > thresh)
					count++;

			return count>1;

		}


	}


}
