package org.pickcellslab.nessys.accuracy;

/*-
 * #%L
 * Nessys
 * %%
 * Copyright (C) 2016 - 2018 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.nessys.accuracy.ComparisonBuilder.Comparison;
import org.pickcellslab.pickcells.api.img.io.ImgIO;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;

/**
 * The base interface for classes capable of measuring differences between a reference label image and a tested label image.
 * 
 */
public interface SegmentationComparator {

	
	/**
	 * A {@link SegmentationComparator} may compare two label images using global features computed from the full images or by comparing pairs of 
	 * individually labelled objects. In the later case, a matching object in the tested image must be found for each individual object found in 
	 * the reference image.
	 * This 'matching' is performed by nessys and this method specifies whether this {@link SegmentationComparator} requires the macthing to 
	 * be done or if the comparison it performs is done on the full images.
	 * <br>
	 * In particular the value returned here will condition inputs provided in 
	 * {@link #addTest(ImgIO, RandomAccessibleInterval, RandomAccessibleInterval)}
	 *  
	 * @return true if this {@link SegmentationComparator} requires matching, false otherwise
	 * 
	 */
	public boolean requiresMatching();
	
	/**
	 * Notifies this {@link SegmentationComparator} that the provided images need to be included within the {@link Comparison} object
	 * returned by the next call to {@link #getResult()} 
	 * <br>
	 * <b>NB:</b> The provided inputs depend on the value returned by {@link #requiresMatching()}. If {@link #requiresMatching()} returns true,
	 * then each image will contain only one label corresponding to a single object in the reference image and to its matching object in the tested
	 * image. If instead, {@link #requiresMatching()} returns false, then the entire reference and tested images will be provided. 
	 * @param io An ImgIO instance to deal  with potentially required input/output operations
	 * @param ref The reference image (same dimensions as tested)
	 * @param tested The tested image (same dimensions as ref)
	 */
	public <R extends NativeType<R> & RealType<R>, T extends NativeType<T> & RealType<T>> void addTest(ImgIO io, RandomAccessibleInterval<R> ref, RandomAccessibleInterval<T> tested);
	
	/**
	 * @return The {@link Comparison} holding the comparison quantification 
	 */
	public Comparison getResult();
	
	/**
	 * Notifies this {@link SegmentationComparator} that any internal state about any ongoing computation of segmentation comparision should be cleared.
	 */
	public void reset();
	
}
