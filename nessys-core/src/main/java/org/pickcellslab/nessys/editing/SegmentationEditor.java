package org.pickcellslab.nessys.editing;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.io.IOException;
import java.util.logging.Level;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.nessys.shared.ImgInputProvider;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;
import org.pickcellslab.pickcells.api.img.view.ImageDisplay;
import org.pickcellslab.pickcells.api.img.view.ImageDisplayFactory;

import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;


public class SegmentationEditor<S extends RealType<S> & NativeType<S>, T extends RealType<T> & NativeType<T>> {


	private final ImgInputProvider inputProvider;
	private final ProviderFactoryFactory pff;
	private final ImgIO io;
	private final ImageDisplayFactory dispFctry;
	private final NotificationFactory notifier;

	private final UITheme theme;




	public SegmentationEditor(
			ImgInputProvider inputProvider, 
			ProviderFactoryFactory pff,
			ImgIO io,
			ImageDisplayFactory dispFctry,
			UITheme theme,
			NotificationFactory notifier){

		this.inputProvider = inputProvider;
		this.theme = theme;
		this.pff = pff;
		this.io = io;
		this.dispFctry = dispFctry;
		this.notifier = notifier;

	}



	public void run() {


		// Get user inputs
		final Image image = inputProvider.getOneImage("Choose the image to be edited", icon());
		if(image==null)
			return;
		final LabelsImage sr = inputProvider.getOneLabels(image, "Choose the corresponding label image", icon());
		if(sr==null)
			return;


		// Now launch the display

		//Create the display
		final ImageDisplay display = dispFctry.newDisplay();

		//Add the color image
		Img<T> color;
		try {
			color = io.open(image);
		} catch (IOException e1) {
			notifier.display("SegmentationEditor Error", "Loading the image has failed", e1, Level.SEVERE);
			return;
		}
		
		display.addImage(image.getMinimalInfo(), color);
		//Name channels
		String[] channelNames = image.channels();
		for(int i=0; i<channelNames.length; i++){
			display.getChannel(0, i).setChannelName(channelNames[i]);
			//TODO display.getChannel(0, i).getAvailableLuts()
		}

		// Add annotations
		SegmentationImageProvider<S> segProvider = null;
		try {
			ProviderFactory pf = pff.create(1);
			pf.addToProduction(sr);
			segProvider = pf.get(sr);
		}
		catch(Exception e){
			notifier.display("SegmentationEditor Error", "Loading the segmentation image has failed", e, Level.SEVERE);
			return;
		}

		final SegmentedObjectManager<S> mgr = new SegmentedObjectManager<S>(segProvider, io, theme, notifier);
		display.addAnnotationLayer("Annotations", mgr);

		/*

			//Save color changes in preferences
			final UserPreferences prefs = CrossDBRessources.getPrefsForCurrentUser(access);
			ColorTable[] luts = ImgDimensions.createColorTables(color.firstElement(), image.luts());			
			AnnotationEditorView<T> view = new AnnotationEditorView<>(manager, color, luts, image.order(), prefs);
		 */

		JToolBar toolBar = SegmentationEditorControllers.createControls(mgr, display);

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(toolBar,BorderLayout.NORTH);
		panel.add(display.getView(), BorderLayout.CENTER);

		JFrame f = new JFrame();
		f.setContentPane(panel);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);

		display.refreshDisplay();	

	}

	

	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/SegEditor_32.png"));
	}


	public String name() {
		return "Segmentation Editor";
	}

	public String description() {
		return "<HTML>Segmentation Editor: View and edit segmentation results.</HTML>";
	}



}
