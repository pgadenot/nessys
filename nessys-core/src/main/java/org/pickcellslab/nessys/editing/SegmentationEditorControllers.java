package org.pickcellslab.nessys.editing;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.Objects;
import java.util.logging.Level;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.img.editors.AnnotationManagerPaintable;
import org.pickcellslab.pickcells.api.img.editors.AnnotationManagerWritable;
import org.pickcellslab.pickcells.api.img.editors.PaintInstance;
import org.pickcellslab.pickcells.api.img.editors.PaintableAnnotation;
import org.pickcellslab.pickcells.api.img.providers.OutOfLabelException;
import org.pickcellslab.pickcells.api.img.view.BrushRadiiPanel;
import org.pickcellslab.pickcells.api.img.view.DisplayPositionListener;
import org.pickcellslab.pickcells.api.img.view.ImageDisplay;
import org.pickcellslab.pickcells.api.img.view.MouseDisplayEvent;
import org.pickcellslab.pickcells.api.img.view.MouseDisplayEventListener;
import org.pickcellslab.pickcells.api.img.view.MouseDisplayEventModel.SelectionMode;

import com.alee.managers.notification.NotificationIcon;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;
import com.alee.managers.tooltip.TooltipManager;

import net.imglib2.realtransform.AffineGet;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.ui.TransformListener;

public final class SegmentationEditorControllers {


	private static final String description = 
			"<HTML><h3>View Controls (Always Active) :</h3>"
					+"<ul>"
					+"<li>Clear the current Selection :&nbsp; <strong>Shift + C</strong></li>"
					+"<li>Inverse the current selection :<strong> Shift + I</strong></li>"
					+"<li>Toggle Annotation visibility : <strong>Shift + S</strong></li>"
					+"<li>Move 1 Z slice up : <strong>Up arrow</strong></li>"
					+"<li>Move 1 Z slice down :<strong> Down arrow</strong></li>"
					+"</ul>"
					+"<h3>Default Controls :</h3>"
					+"<ul>"
					+"<li>Delete : <strong>Ctrl + D</strong></li>"
					+"<li>Merge : <strong>Ctrl + M</strong></li>"
					+"<li>Split at current slice : <strong>Ctrl + /</strong></li>"
					+"<li>Validate Changes : <strong>Ctrl + V</strong></li>"
					+"</ul>"
					+"<h3>Other Controls (Eraser, Expander, Adder, Splitter) :</h3>"
					+"<p>These 4 controls use the same principle: First select an object to edit</p>"
					+"<p>(or in the case of Adder click in an empty space), the cursor shape will</p>"
					+"<p>change and you will be able to start 'drawing' in the 3 dimensions to either</p>"
					+"<p>erase or expand the selected object or create a new object with the Adder control.</p>"
					+"<ul>"
					+"<li>Change Brush Size : <strong>Shift + D</strong></li>"
					+"<li>Validate Changes : <strong>Ctrl + V</strong></li>"
					+"<li>Undo :<strong> Ctrl + Z</strong></li>"
					+"<li>Redo :<strong> Ctrl + Shift + Z</strong></li>"
					+"</ul></HTML>";






	private SegmentationEditorControllers(){}








	public static JToolBar createControls(AnnotationManagerPaintable<?,?> mgr, ImageDisplay view){

		Objects.requireNonNull(mgr, "manager is null");
		Objects.requireNonNull(view, "view is null");

		view.getMouseModel().setRotationEnabled(false);


		// Create the toolBar

		JToolBar toolBar = new JToolBar();


		// Controls switch
		final String[] cts = new String[]{"Defaults (F1)", "Undo (F2)", "Eraser (F3)", "Expander (F4)", "Adder (F5)", "Splitter (F6)" };
		final JComboBox<String> controls = new JComboBox<>(cts);

		controls.addActionListener(l->{
			if(controls.getSelectedItem() == cts[0]){
				SegmentationEditorControllers.switchToDefaultControl(mgr, view);
			}
			else if(controls.getSelectedItem() == cts[1]){
				SegmentationEditorControllers.removeDefaultKeyBinding(view);
				view.getMouseModel().setSelectionMode(SelectionMode.NONE);
				MouseDisplayEventListener ml = view.getMouseModel().setControl(new UndoControl(mgr, controls));
				if (ml!=null && Deregisterable.class.isAssignableFrom(ml.getClass())){
					((Deregisterable) ml).deregister();
				}
				//System.out.println("Undo Control Selected");
				WebNotification wn = NotificationManager.showNotification(view.getView(), "Undo Control Selected", NotificationIcon.information.getIcon());
				wn.setDisplayTime(1000);
			}
			else if(controls.getSelectedItem() == cts[2]){
				SegmentationEditorControllers.removeDefaultKeyBinding(view);
				view.getMouseModel().setSelectionMode(SelectionMode.NONE);
				MouseDisplayEventListener ml = view.getMouseModel().setControl(new EraseControl<>(view, mgr, controls));
				if (ml!=null && Deregisterable.class.isAssignableFrom(ml.getClass())){
					((Deregisterable) ml).deregister();
				}
				//System.out.println("Eraser Control Selected");
				WebNotification wn = NotificationManager.showNotification(view.getView(), "Eraser Control Selected", NotificationIcon.information.getIcon());
				wn.setDisplayTime(1000);
			}
			else if(controls.getSelectedItem() == cts[3]){
				SegmentationEditorControllers.removeDefaultKeyBinding(view);
				view.getMouseModel().setSelectionMode(SelectionMode.NONE);
				MouseDisplayEventListener ml = view.getMouseModel().setControl(new ExpandControl<>(view, mgr, controls));
				if (ml!=null && Deregisterable.class.isAssignableFrom(ml.getClass())){
					((Deregisterable) ml).deregister();
				}
				//System.out.println("Expander Control Selected");
				WebNotification wn = NotificationManager.showNotification(view.getView(), "Expander Control Selected", NotificationIcon.information.getIcon());
				wn.setDisplayTime(1000);
			}
			else if(controls.getSelectedItem() == cts[4]){
				SegmentationEditorControllers.removeDefaultKeyBinding(view);
				view.getMouseModel().setSelectionMode(SelectionMode.NONE);
				MouseDisplayEventListener ml = view.getMouseModel().setControl(new AdditionControl<>(view, mgr, controls));
				if (ml!=null && Deregisterable.class.isAssignableFrom(ml.getClass())){
					((Deregisterable) ml).deregister();
				}
				view.getNotificationFactory().notify(view.getView(), "Addition  Control Selected");
			}
			else if(controls.getSelectedItem() == cts[5]){
				SegmentationEditorControllers.removeDefaultKeyBinding(view);
				view.getMouseModel().setSelectionMode(SelectionMode.NONE);
				MouseDisplayEventListener ml = view.getMouseModel().setControl(new SplitterControl<>(view, mgr, controls));
				if (ml!=null && Deregisterable.class.isAssignableFrom(ml.getClass())){
					((Deregisterable) ml).deregister();
				}
				view.getNotificationFactory().notify(view.getView(), "Splitter Control Selected");
			}
		});


		controls.setSelectedIndex(0);

		toolBar.add(new JLabel("Current Control : "));
		toolBar.add(controls);



		JButton sc = new JButton("Help");
		TooltipManager.setTooltip(sc, "Display the Keyboard Controls");
		sc.addActionListener(l->{
			JEditorPane txt = new JEditorPane();
			txt.setContentType( "text/html" );
			txt.setText(description);
			txt.setEditable(false);
			JOptionPane.showMessageDialog(view.getView(),txt);
		});


		toolBar.add(sc);


		// Bind Controls
		SegmentationEditorControllers.bindControl(view, controls, "F1", cts[0], 0);
		SegmentationEditorControllers.bindControl(view, controls, "F2", cts[1], 1);
		SegmentationEditorControllers.bindControl(view, controls, "F3", cts[2], 2);
		SegmentationEditorControllers.bindControl(view, controls, "F4", cts[3], 3);
		SegmentationEditorControllers.bindControl(view, controls, "F5", cts[4], 4);
		SegmentationEditorControllers.bindControl(view, controls, "F6", cts[5], 5);




		// Save and commit to the database
		JButton save = new JButton(view.getIconTheme().icon(IconID.Files.SAVE, 16));
		TooltipManager.setTooltip(save, "Saves the segmentation result image (overwrite) and commits the editing counts.");
		save.addActionListener(l->{


			Thread t = new Thread(()->{
				try{
					mgr.save();						
				} catch (Exception e) {
					view.getNotificationFactory().display("Error", "An error occured while saving the image", e, Level.WARNING);
				}						
			});					
			view.getNotificationFactory().waitAnimation(t, "Saving changes in the image");				
		});


		toolBar.add(save);




		// Annotation Visibility
		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("shift S"), "Annotations", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				mgr.toggleAnnotationsVisible();				
			}			
		});




		return toolBar;

	}









	private static <T extends RealType<T> & NativeType<T>> void removeDefaultKeyBinding(ImageDisplay view){
		Objects.requireNonNull(view, "View is null");
		view.getKeyStrokeModel().removeKeyBinding(KeyStroke.getKeyStroke("ctrl D"));
		view.getKeyStrokeModel().removeKeyBinding(KeyStroke.getKeyStroke("ctrl SLASH"));
		view.getKeyStrokeModel().removeKeyBinding(KeyStroke.getKeyStroke("ctrl M"));
		view.getKeyStrokeModel().removeKeyBinding(KeyStroke.getKeyStroke("ctrl V"));
	}






	@SuppressWarnings("serial")
	private static void switchToDefaultControl(AnnotationManagerPaintable<?,?> mgr, ImageDisplay view){

		Objects.requireNonNull(mgr, "mgr is null");
		Objects.requireNonNull(view, "View is null");

		//System.out.println("Default Control Selected");

		WebNotification wn = NotificationManager.showNotification(view.getView(), "Default Control Selected", NotificationIcon.information.getIcon());
		wn.setDisplayTime(1000);

		// Deregister the current mouseListener
		MouseDisplayEventListener ml = view.getMouseModel().setControl(null);			
		if (ml!=null && Deregisterable.class.isAssignableFrom(ml.getClass())){
			((Deregisterable) ml).deregister();
		}


		view.getMouseModel().setSelectionMode(SelectionMode.MULTIPLE);

		// Register Del / Merge / Split / Validate key bindings
		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl D"), "Delete", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				mgr.setSelectedAsDeleted();				
			}			
		});

		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl M"), "Merge", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				mgr.mergeSelection();				
			}			
		});	


		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl SLASH"), "Split Z", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				mgr.splitZ(view.currentZSlice());				
			}			
		});	


		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl V"), "Validate", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				mgr.commitChanges();
				System.out.println("SegmentationEditorControlles: Commit Changes done!");
			}			
		});	


	}








	@SuppressWarnings("serial")
	private static void bindControl(ImageDisplay view, JComboBox<String> cb, String key, String id, int index){

		Objects.requireNonNull(view, "View is null");

		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke(key), id, new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				cb.setSelectedIndex(index);
			}			
		});		
	}




	private interface Deregisterable {
		void deregister();
	}




	private static class UndoControl extends MouseDisplayEventListener {

		private final AnnotationManagerWritable mgr;
		private final JComboBox box;

		public UndoControl( AnnotationManagerWritable mgr, JComboBox box) {
			this.mgr = mgr;			
			this.box = box;
		}

		@Override
		public void mouseClicked(MouseDisplayEvent e) {
			if(SwingUtilities.isLeftMouseButton(e.getMouseEvent())){
				if(mgr.undoAt(e.getAnnotationEventLocation(0)))
					box.setSelectedIndex(0);
			}
		}

		//Override hashtable called method for removal
		@Override
		public int hashCode(){
			return UndoControl.class.hashCode();
		}

		@Override
		public boolean equals(Object o){
			return UndoControl.class.equals(o.getClass());
		}


	}







	private static abstract class PaintControl<P,D> extends MouseDisplayEventListener implements Deregisterable, DisplayPositionListener, TransformListener<AffineGet>{


		private static final String Undo = "undo", Redo = "redo";

		private static final String[] dimNames = {"X", "Y", "Z"};

		protected final ImageDisplay view;
		protected final AnnotationManagerPaintable<P,D> mgr;
		protected final JComboBox box;

		/**
		 * The dimensions of the brush
		 */
		protected static long[] dims;		

		private PaintInstance<P,D> epi = null;
		private boolean inDrag;







		public PaintControl(ImageDisplay view, AnnotationManagerPaintable<P,D> mgr, JComboBox box) {

			this.view = view;
			this.mgr = mgr;	
			this.box = box;

			view.getImageView().setCursor(getInitialCursor());
			view.addDisplayPositionListener(this);

		}


		protected abstract Cursor getInitialCursor();

		protected abstract boolean checkValid(PaintableAnnotation<P,D> annotation);

		protected abstract PaintInstance<P,D> createPaintInstance() throws OutOfLabelException;

		protected abstract PaintControl<P,D> newControl();



		@SuppressWarnings("serial")
		@Override
		public void mouseClicked(MouseDisplayEvent e) {

			if(SwingUtilities.isLeftMouseButton(e.getMouseEvent())){

				// First check if anything exists at the click position

				long[] clickPos = e.getAnnotationEventLocation(0);
				PaintableAnnotation<P,D> annotation = mgr.getAnnotationAt(clickPos);

				if(!checkValid(annotation))
					return;


				// Pick the label if not yet defined and change cursor
				if(epi == null){

					if(dims == null)					
						dims = mgr.brushFactories().get(0).defaultBrushDimensions();


					//System.out.println("PaintControl Dims length : "+dims.length);

					try {

						epi = createPaintInstance();


					} catch (OutOfLabelException e1) {
						// If we are out of labels, we need to request a conversion of the image into a higher bit depth.
						Thread t =  new Thread(()->{
							try {
								mgr.increaseLabelsPool();
							} catch (OutOfLabelException e2) {
								view.getNotificationFactory().display("Error", e2.getMessage(), e2, Level.SEVERE);
							}
						});


						try {							

							// Display a progress bar explaining what is going on
							view.getNotificationFactory().waitAnimation(t, "Converting Segmentation");
							t.join();							
							epi = createPaintInstance();
							System.out.println("SegmentationEditorControllers: conversion done and epi reassigned!");


						} catch (Exception e2) {
							throw new RuntimeException(e2);
						}

					}






					view.getImageView().setCursor(this.getCursor(view.getCurrentTransform()));
					view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl Z"), Undo,  new AbstractAction(){
						@Override
						public void actionPerformed(ActionEvent e) {
							epi.undo();
							view.refreshDisplay();
						}			
					});	

					view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl shift Z"), Redo,  new AbstractAction(){
						@Override
						public void actionPerformed(ActionEvent e) {
							epi.redo();
							view.refreshDisplay();
						}			
					});

					view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl V"), "Make",  new AbstractAction(){
						@Override
						public void actionPerformed(ActionEvent e) {
							mgr.commitChanges();
							epi.clear();
							box.setSelectedIndex(0);
							//PaintControl.this.deregister();
							//view.getMouseModel().setControl(PaintControl.this.newControl());
						}			
					});	

					view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("shift D"), "Dialog",  new AbstractAction(){
						@Override
						public void actionPerformed(ActionEvent e) {
							if(epi!=null){
								BrushRadiiPanel brp = new BrushRadiiPanel(dims, dimNames);
								int i = JOptionPane.showConfirmDialog(view.getView(), brp, "Brush Dimensions...", JOptionPane.OK_CANCEL_OPTION);
								if(i == JOptionPane.OK_OPTION){
									dims = brp.getNewDims();
									mgr.brushFactories().get(0).setDefaultBrushDimensions(dims);
									epi.setBrush(epi.getBrush().createWithDims(dims));
									view.getImageView().setCursor(getCursor(view.getCurrentTransform()));
								}
							}
							else
								System.out.println("Epi  is null");
						}			
					});	


					view.addTransformListener(this);
					view.addDisplayPositionListener(this);

				}
				else{
					epi.draw(e.getAnnotationEventLocation(0));
					view.refreshDisplay();
				}

			}
		}






		@Override
		public void mousePressed(MouseDisplayEvent e) {

			if(epi == null)
				return;

			if(SwingUtilities.isLeftMouseButton(e.getMouseEvent())){
				epi.draw(e.getAnnotationEventLocation(0));
				view.refreshDisplay();
				inDrag = true;
			}
		}


		@Override
		public void transformChanged( AffineGet transform ){
			view.getImageView().setCursor(getCursor(transform));
		}


		@Override
		public void positionHasChanged(ImageDisplay view, DisplayPosition dp) {		
			if(dp == DisplayPosition.FRAME)
				this.deregister();
		}



		@Override
		public void mouseReleased(MouseDisplayEvent e) {
			inDrag = false;
		}

		@Override
		public void mouseDragged(MouseDisplayEvent e) {
			if(!inDrag)
				return;
			epi.draw(e.getAnnotationEventLocation(0));
			view.refreshDisplay();	//TODO remove should be automated by listener approach		
		}







		private Cursor getCursor(AffineGet transform){
			//	System.out.println("transform : x = "+transform.get(0, 0));
			int w = (int) (transform.get(0, 0) * epi.getBrushSizeX()); // Rotation is disabled
			int h = (int) (transform.get(1, 1) * epi.getBrushSizeY());			
			java.awt.Image image = epi.getBrushIcon().getImage().getScaledInstance(w,h, java.awt.Image.SCALE_FAST);
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Cursor c = toolkit.createCustomCursor(image , new Point(w/2, h/2), "Painter ");
			return c;
		}



		@Override
		public void deregister() {
			//System.out.println("Paint Control deregistered");
			if(epi!=null)
				epi.cancel();
			epi = null;
			view.getImageView().setCursor(Cursor.getDefaultCursor());
			view.getKeyStrokeModel().removeKeyBinding(KeyStroke.getKeyStroke("ctrl shift Z"));
			view.getKeyStrokeModel().removeKeyBinding(KeyStroke.getKeyStroke("ctrl Z"));
			view.getKeyStrokeModel().removeKeyBinding(KeyStroke.getKeyStroke("ctrl V"));
			view.getKeyStrokeModel().removeKeyBinding(KeyStroke.getKeyStroke("shift D"));
			view.removeTransformListener(this);
			view.removeDisplayPositionListener(this);
		}


	}





	private static class EraseControl<P,D> extends PaintControl<P,D>{



		private PaintableAnnotation<P,D> annotation;


		public EraseControl(ImageDisplay view, AnnotationManagerPaintable<P,D> mgr, JComboBox box) {
			super(view,mgr, box);
		}




		@Override
		protected Cursor getInitialCursor() {
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			java.awt.Image image = UITheme.toImageIcon(view.getIconTheme().icon(IconID.Misc.PIPETTE, 16, Color.YELLOW)).getImage();
			Cursor c = toolkit.createCustomCursor(image , new Point(), "ColorPickcer");
			return c;
		}




		@Override
		protected boolean checkValid(PaintableAnnotation<P,D> annotation) {
			if(annotation == null)
				return false;
			else{
				this.annotation = annotation;
				return true;
			}
		}




		@Override
		protected PaintInstance<P,D> createPaintInstance() {
			return mgr.brushFactories().get(0).createEraserFor(annotation);
		}




		@Override
		protected PaintControl<P,D> newControl() {
			return new EraseControl<>(view,mgr, box);
		}


	}





	private static class ExpandControl<P,D> extends PaintControl<P,D>{



		private PaintableAnnotation<P,D> annotation;


		public ExpandControl(ImageDisplay view, AnnotationManagerPaintable<P,D> mgr, JComboBox box) {
			super(view,mgr, box);
		}




		@Override
		protected Cursor getInitialCursor() {
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			java.awt.Image image = UITheme.toImageIcon(view.getIconTheme().icon(IconID.Misc.PIPETTE, 16, Color.YELLOW)).getImage();
			Cursor c = toolkit.createCustomCursor(image , new Point(), "ColorPickcer");
			return c;
		}




		@Override
		protected boolean checkValid(PaintableAnnotation<P,D> annotation) {
			if(annotation == null)
				return false;
			else{
				this.annotation = annotation;
				return true;
			}
		}




		@Override
		protected PaintInstance<P,D> createPaintInstance() {
			return mgr.brushFactories().get(0).createExpanderFor(annotation);
		}




		@Override
		protected PaintControl<P,D> newControl() {
			return new ExpandControl<>(view,mgr, box);
		}


	}









	private static class AdditionControl<P,D> extends PaintControl<P,D>{



		public AdditionControl(ImageDisplay view, AnnotationManagerPaintable<P,D> mgr, JComboBox box) {
			super(view,mgr, box);
		}




		@Override
		protected Cursor getInitialCursor() {
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			java.awt.Image image = UITheme.toImageIcon(view.getIconTheme().icon(IconID.Misc.PIPETTE, 16, Color.YELLOW)).getImage();
			Cursor c = toolkit.createCustomCursor(image , new Point(), "ColorPickcer");
			return c;
		}




		@Override
		protected boolean checkValid(PaintableAnnotation<P,D> annotation) {
			return annotation == null;
		}




		@Override
		protected PaintInstance<P,D> createPaintInstance() throws OutOfLabelException {
			return mgr.brushFactories().get(0).createAnnotationPainter(mgr, view.currentTimeFrame());
		}




		@Override
		protected PaintControl<P,D> newControl() {
			return new AdditionControl<>(view,mgr, box);
		}


	}



	private static class SplitterControl<P,D> extends PaintControl<P,D>{



		private PaintableAnnotation<P,D> annotation;


		public SplitterControl(ImageDisplay view, AnnotationManagerPaintable<P,D> mgr, JComboBox box) {
			super(view,mgr, box);
		}




		@Override
		protected Cursor getInitialCursor() {
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			java.awt.Image image = UITheme.toImageIcon(view.getIconTheme().icon(IconID.Misc.PIPETTE, 16, Color.YELLOW)).getImage();
			Cursor c = toolkit.createCustomCursor(image , new Point(), "ColorPickcer");
			return c;
		}




		@Override
		protected boolean checkValid(PaintableAnnotation<P,D> annotation) {
			if(annotation == null)
				return false;
			else{
				this.annotation = annotation;
				return true;
			}
		}




		@Override
		protected PaintInstance<P,D> createPaintInstance() throws IllegalArgumentException, OutOfLabelException {
			return mgr.brushFactories().get(0).createSplitterFor(annotation);
		}




		@Override
		protected PaintControl<P,D> newControl() {
			return new SplitterControl<>(view,mgr,box);
		}


	}




}
