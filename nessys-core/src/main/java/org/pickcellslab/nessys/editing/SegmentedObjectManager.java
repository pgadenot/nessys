package org.pickcellslab.nessys.editing;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JOptionPane;

import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.threads.ModeratedThread;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedPrototype;
import org.pickcellslab.pickcells.api.img.editors.AnnotationManagerPaintable;
import org.pickcellslab.pickcells.api.img.editors.EllipsePaintInstanceFactory;
import org.pickcellslab.pickcells.api.img.editors.PaintInstance;
import org.pickcellslab.pickcells.api.img.editors.PaintInstanceConsumer;
import org.pickcellslab.pickcells.api.img.editors.PaintInstanceFactory;
import org.pickcellslab.pickcells.api.img.editors.PaintInstanceListener;
import org.pickcellslab.pickcells.api.img.editors.PaintableAnnotation;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.process.Outline;
import org.pickcellslab.pickcells.api.img.providers.OutOfLabelException;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;
import org.pickcellslab.pickcells.api.img.view.AbstractAnnotationManager;
import org.pickcellslab.pickcells.api.img.view.Annotation;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatusListener;
import org.pickcellslab.pickcells.api.img.view.DisplayMode;
import org.pickcellslab.pickcells.api.img.view.ImageDisplay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.Cursor;
import net.imglib2.FinalInterval;
import net.imglib2.Interval;
import net.imglib2.Point;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.outofbounds.OutOfBoundsConstantValueFactory;
import net.imglib2.outofbounds.OutOfBoundsMirrorFactory;
import net.imglib2.roi.labeling.BoundingBox;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.ARGBType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.ByteType;
import net.imglib2.util.Intervals;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;



public class SegmentedObjectManager<T extends RealType<T> & NativeType<T>> extends AbstractAnnotationManager implements AnnotationManagerPaintable<ByteType,T> {



	public enum Status implements AnnotationStatus{
		DELETED{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(255, 0, 0, 255);
			}

		},
		ADDED{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(0, 255, 0, 255);
			}

		},
		MERGED{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(255, 0, 255, 255);
			}

		},
		SPLIT{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(0, 0, 255, 255);
			}

		}, 
		AMENDED{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(255, 255, 0, 255);
			}

		}, 
		ERASED{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(0, 0, 0, 255);
			}

		}, 
		EFFECTIVELY_DELETED{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(0, 0, 0, 255);
			}

		}, 
		EXPANDED{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(0, 255, 0, 255);
			}

		}, 
		SPLITTED{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(0, 0, 255, 255);
			}

		};

	}


	private static final Logger log = LoggerFactory.getLogger(SegmentedObjectManager.class);

	private static final ByteType BG = new ByteType(), MASK = new ByteType((byte) 1), NOVEL = new ByteType((byte)2), ERASE = new ByteType((byte)3), EXPAND = new ByteType((byte)4) , SPLIT = new ByteType((byte)5);



	private final SegmentationImageProvider<T> provider;

	private final PaintInstanceFactory<ByteType,T> piFctry;

	private final ImgIO io;


	/*-------------------- Current Position ---------------------------------*/
	private int zPos = 0;
	private int tPos = 0;

	/*-------------------- Cache ---------------------------------*/
	/**
	 * Store masks of shape outlines for each time frame.
	 */
	private final Map<Integer,RandomAccessibleInterval<ByteType>> masks = new ConcurrentHashMap<>();
	private final Map<Integer,Map<Object,AnnotationEdit<T,?>>> timePoints = new ConcurrentHashMap<>();   


	private RandomAccessibleInterval<ByteType> maskPlane;
	private RandomAccessibleInterval<T> labelPlane;



	/*-------------------- Ref to objects being modified ---------------------------------*/
	private final Set<PaintedAnnotation> additions = new HashSet<>();
	private final Set<EditedAnnotation> editions = new HashSet<>();
	private final List<MergedAnnotation> merges = new ArrayList<>();
	private final List<SplitAnnotation> splits = new ArrayList<>();


	/*-------------------- Mask Cache updater  ---------------------------------*/
	// TODO create dedicated Class which can be listened for loading process and pass ref of this object using interface (setPlanes())
	private final ModeratedThread modThread = new ModeratedThread(()->checkMask());


	private final AnnotationDecoy decoy = new AnnotationDecoy();


	/*-------------------- Listener  ---------------------------------*/
	private final List<AnnotationStatusListener> statusLstrs = new ArrayList<>();

	private final NotificationFactory notifier;




	public SegmentedObjectManager(SegmentationImageProvider<T> provider, ImgIO io, UITheme theme, NotificationFactory notifier) {
		Objects.requireNonNull(provider);
		this.provider = provider;
		provider.setPrototype(provider.origin(), SegmentedObjectManager.createPrototype(), false);
		this.notifier = notifier;
		this.io = io;
		long[]annotationDims = this.annotatedImageInfo().removeDimension(Image.t).imageDimensions();
		Arrays.fill(annotationDims, 3);
		this.piFctry = new EllipsePaintInstanceFactory<>(theme, annotationDims);
		labelPlane = provider.getSlice(0,0);
		maskPlane = io.createImg(labelPlane, BG);
		modThread.start();
	}







	@Override
	public List<PaintInstanceFactory<ByteType,T>> brushFactories() {
		return Collections.singletonList(piFctry);
	}








	@Override
	public List<AnnotationStatus> possibleAnnotationStates() {
		List<AnnotationStatus> list = new ArrayList<>();
		list.add(AnnotationStatus.DESELECTED);
		list.add(AnnotationStatus.SELECTED);		
		for(Status s : Status.values())
			list.add(s);
		return list;
	}






	@Override
	public Map<String, List<Dimension<Annotation, ?>>> possibleAnnotationDimensions(Predicate<Dimension> filter, boolean decompose) {

		final SegmentedObject so = provider.getAnyItem().orElse(null);
		if(null == so)
			return Collections.emptyMap();

		final List<Dimension<Annotation, ?>> list = new ArrayList<>();

			if(!decompose)
				so.getValidAttributeKeys().forEach(k->{
					final Dimension<Annotation, ?> dim = Annotation.createDimension((AKey)k, so.getAttribute(k).get());
					if(filter.test(dim))
						list.add(dim);
				});

			else
				so.getValidAttributeKeys().forEach(k->{
					final List<Dimension<Annotation, ?>> dims = Annotation.createDecomposedDimension((AKey)k, so.getAttribute(k).get());
					for(Dimension<Annotation, ?> dim : dims)
						if(filter.test(dim))
							list.add(dim);
				});		

		return Collections.singletonMap(so.declaredType(), list);
	}

	
	
	
	@Override
	public PaintableAnnotation<ByteType,T> getAnnotationAt(long[] clickPos) {
		if(masks.get(tPos) == null)	return null;
		if(timePoints.get(tPos) == null) return null;
		final Point p = new Point(clickPos);
		if(!Intervals.contains(masks.get(tPos), p))
			return null;

		final Optional<SegmentedObject> opt = provider.getData(p);
		if(opt.isPresent()){
			Float l = opt.get().label().get();
			AnnotationEdit<T, ?> sa = timePoints.get(tPos).get(l);
			if(sa == null){
				sa = new SegmentedAnnotation(opt.get());
				timePoints.get(tPos).put(l, sa);
			}
			return sa;
		}
		return null;
	}






	@Override
	public synchronized void toggleSelectedAt(long[] pos) {

		final Point p = new Point(pos);
		if(!Intervals.contains(masks.get(tPos), p))
			return;

		System.out.println("toggle selected at Position -> "+p);
		Optional<SegmentedObject> obj = provider.getData(p);
		if(obj.isPresent()){
			BoundingBox bb = new BoundingBox(2);
			// Check if it is already in timePoints.get(tPos)
			AnnotationEdit<T, ?> sa = timePoints.get(tPos).get(obj.get().label().get());
			if(sa != null){
				sa.toggleSelected();
				sa.updateBoundingBox(bb);
				this.fireAnnotationRedrawRequired(bb);
			}
			else{
				sa = new SegmentedAnnotation(obj.get());
				sa.toggleSelected();
				timePoints.get(tPos).put(obj.get().label().get(), sa);
				sa.updateBoundingBox(bb);
				this.fireAnnotationRedrawRequired(bb);
			}
		}
	}


	@Override
	public synchronized void toggleSelected(Annotation previous) {
		// Check if it is already in timePoints.get(tPos)
		System.out.println("toggel annotation -> "+previous);
		@SuppressWarnings("unchecked")
		AnnotationEdit<T,?> sa = (AnnotationEdit<T,?>) previous;
		if(sa != null){
			final BoundingBox bb = new BoundingBox(2);
			sa.toggleSelected();
			sa.updateBoundingBox(bb);
			System.out.println("SegmentedResultEditorManager : selection toggled for "+sa);
			this.fireAnnotationRedrawRequired(bb);
		}
	}




	@SuppressWarnings("unchecked")
	@Override
	public void inverseSelection() {
		this.stream("").forEach(a->{
			AnnotationEdit<T,?> sa = (AnnotationEdit<T,?>)a;
			sa.toggleSelected();
		});
		lstrs.forEach(l->l.annotationsReDrawRequired(maskPlane));
	}



	@Override
	public void clearSelection(){
		System.out.println("Selection cleared");
		final BoundingBox bb = new BoundingBox(2);
		AtomicBoolean b = new AtomicBoolean(false);
		timePoints.get(tPos).values().forEach(a->{
			if(a.status() == Status.SELECTED){
				a.toggleSelected();
				a.updateBoundingBox(bb);
				b.set(true);
			}
		});
		if(b.get())
			this.fireAnnotationRedrawRequired(bb);
	}




	@Override
	public void setDragged(Annotation a, long[] newPos) {}





	@Override
	public boolean undoAt(long[] pos) {

		//System.out.println("Undo for pos "+Arrays.toString(pos));	

		Optional<SegmentedObject> obj = provider.getData(new Point(pos));
		if(obj.isPresent()){ 

			Float label = obj.get().label().get();
			AnnotationEdit<T,?> sa = timePoints.get(tPos).remove(label);
			if(sa != null){				
				if(sa.undo()){
					System.out.println("Undone : "+sa.status());
					final BoundingBox bb = new BoundingBox(2);
					sa.updateBoundingBox(bb);
					this.fireAnnotationRedrawRequired(bb);	
					return true;
				}
			}
		}		

		return false;
	}





	@Override
	public void setSelectedAsDeleted() {
		System.out.println("Selection deleted");
		final BoundingBox bb = new BoundingBox(2);
		timePoints.get(tPos).values().forEach(a->{
			if(a.status() == Status.SELECTED){
				if(a.setDeleted(true))
					a.updateBoundingBox(bb);
			}
		});
		this.fireAnnotationRedrawRequired(bb);
	}



	@Override
	public void undeleteAll() {
		System.out.println("Undelete all");
		final BoundingBox bb = new BoundingBox(2);
		final AtomicBoolean fire = new AtomicBoolean(false);
		new ArrayList<>(timePoints.get(tPos).values()).forEach(a->{
			if(a.status() == Status.DELETED){
				a.setDeleted(false);
				a.updateBoundingBox(bb);
				fire.set(true);
			}
		});
		if(fire.get())
			this.fireAnnotationRedrawRequired(bb);
	}



	@SuppressWarnings("unchecked")
	@Override
	public void mergeSelection() {
		System.out.println("Merge Selection");
		Set<SegmentedAnnotation> toMerge = new HashSet<>();
		timePoints.get(tPos).values().forEach(a->{
			if(a.status() == Status.SELECTED){
				toMerge.add((SegmentedAnnotation) a);
			}
		});
		if(toMerge.isEmpty())	return;

		MergedAnnotation ma = null;
		try {
			ma = new MergedAnnotation(toMerge, provider.newLabel(tPos));
		} catch (OutOfLabelException e) {
			e.printStackTrace();
			return;
		}

		this.fireAnnotationRedrawRequired(ma.getBoundingBox());

	}






	@Override
	public void splitZ(long slice) {
		final BoundingBox bb = new BoundingBox(2);
		final List<SplitAnnotation> list = new ArrayList<>();
		timePoints.get(tPos).values().forEach(a->{
			if(a.status() == Status.SELECTED){
				@SuppressWarnings("unchecked")
				SegmentedAnnotation sa = (SegmentedAnnotation) a;
				SplitAnnotation ma = new SplitAnnotation(sa, slice);
				list.add(ma);
				ma.updateBoundingBox(bb);
			}
		});		
		if(list.isEmpty())	return; 
		this.fireAnnotationRedrawRequired(bb);
	}






	@Override
	public void addEraser(PaintInstance<ByteType,T> pi) {
		final AnnotationEdit<?, ?> ae = (AnnotationEdit<?, ?>) pi.getAnnotation();
		final BoundingBox bb = new BoundingBox(2);
		ae.updateBoundingBox(bb);
		this.lstrs.forEach(l->l.annotationsReDrawRequired(bb));
		new EditedAnnotation(pi,ERASE);	
	}


	@Override
	public void addExpander(PaintInstance<ByteType,T> pi) {
		final AnnotationEdit<?, ?> ae = (AnnotationEdit<?, ?>) pi.getAnnotation();
		final BoundingBox bb = new BoundingBox(2);
		ae.updateBoundingBox(bb);
		this.lstrs.forEach(l->l.annotationsReDrawRequired(bb));
		new EditedAnnotation(pi, EXPAND);
	}


	@Override
	public void addSplitter(PaintInstance<ByteType,T> pi) {	
		final AnnotationEdit<?, ?> ae = (AnnotationEdit<?, ?>) pi.getAnnotation();
		final BoundingBox bb = new BoundingBox(2);
		ae.updateBoundingBox(bb);
		this.lstrs.forEach(l->l.annotationsReDrawRequired(bb));
		new EditedAnnotation(pi, SPLIT);			
	}


	@Override
	public void addAnnotation(PaintInstance<ByteType,T> pi) {
		PaintedAnnotation a = (PaintedAnnotation) pi.getAnnotation();
		a.setPaintInstance(pi);
	}




	@Override
	public RandomAccessibleInterval<T> getDataFrame(Annotation a) {
		return provider.getFrame(((AnnotationEdit<?,?>)a).frame());
	}


	@Override
	public RandomAccessibleInterval<ByteType> getPaintableFrame(Annotation a) {
		return masks.get(((AnnotationEdit<?,?>)a).frame());
	}




	@Override
	public T dataValue(Annotation a) {
		return provider.variable(((SegmentedAnnotation)a).data().label().get());
	}


	@Override
	public T nullDataValue() {
		return provider.variable(0f);
	}


	@Override
	public ByteType eraseValue(Annotation a) {
		return ERASE;
	}


	@Override
	public ByteType expandValue(PaintableAnnotation<ByteType,T> a) {
		return EXPAND;
	}


	@Override
	public ByteType splitValue(PaintableAnnotation<ByteType,T> a) {
		return SPLIT;
	}


	@Override
	public ByteType createValue(PaintableAnnotation<ByteType,T> a) {
		return NOVEL;
	}


	@Override
	public ByteType nullPaintValue() {
		return BG;
	}


	@Override
	public PaintableAnnotation<ByteType,T> newPaintable(int time) throws OutOfLabelException {
		return new PaintedAnnotation(time, provider.newLabel(time));		
	}




	@Override
	public void increaseLabelsPool() throws OutOfLabelException {

		if(JOptionPane.NO_OPTION ==
				JOptionPane.showConfirmDialog(null, "The segmented image must be converted to a higher bit depth due to the number of"
						+ " shapes already present in the image.\n"
						+ "Current changes will be commited and saved, Do you want to continue?", "Conversion Required...", JOptionPane.YES_NO_OPTION)
				)
			return;

		try {

			this.commitChanges();
			this.save();
			provider.convertToNextBitDepth();
			// Now we need to rebuild cache containing refs to actual image
			labelPlane = provider.getSlice(tPos,zPos);
			maskPlane = io.createImg(labelPlane, BG);
			masks.clear();
			checkMask();

		} catch (IOException e) {
			throw new OutOfLabelException("Unable to increaseLabelsPool", e); //Should we throw RuntimeException instead?
		}		
	}



	@Override
	public void commitChanges() {


		new ArrayList<>(timePoints.get(tPos).values()).forEach(a->a.validate());
		//TODO send changes to LabelsImage
		/*
		// Deletions
		new ArrayList<>(timePoints.get(tPos).values()).forEach(a->{	
			a.validate();
			if(a.status() == Status.DELETED){
				SegmentedAnnotation sa = (SegmentedAnnotation) a;
				sa.setStatus(Status.EFFECTIVELY_DELETED);
				this.fireAnnotationChange(sa);
				provider.delete(sa.data());
				//timePoints.get(tPos).remove(sa.data().label().get());
				LabelsImage li = provider.origin();
				int d = li.getAttribute(deletesKey).orElse(0);
				li.setAttribute(deletesKey,++d);
			}			
		});

		// Merges
		new ArrayList<>(merges).forEach(m->{

			List<SegmentedAnnotation> toDel = new ArrayList<>(m.toMerge);
			toDel.forEach(sa->{
				sa.setStatus(Status.EFFECTIVELY_DELETED);
				//timePoints.get(tPos).remove(sa.id.label().get());
			});
			this.fireAnnotationChange(toDel);

			List<SegmentedObject> toMerge = toDel.stream().map(sa->sa.data()).collect(Collectors.toList());			
			SegmentedObject so = provider.merge(toMerge);
			if(so!=null){
				SegmentedAnnotation sa = new SegmentedAnnotation(so);
				this.fireAnnotationChange(sa);
			}

			LabelsImage li = provider.origin();
			int d = li.getAttribute(mergesKey).orElse(0);
			li.setAttribute(mergesKey,++d);

			merges.remove(m);

		});


		// Splits
		new ArrayList<>(splits).forEach(s->{			

			try{

				Pair<SegmentedObject,SegmentedObject> pair = provider.splitZ(s.toSplit.data, s.slice);
				List<SegmentedAnnotation> splitted = new ArrayList<>(2);
				splitted.add(new SegmentedAnnotation(pair.getFirst()));
				splitted.add(new SegmentedAnnotation(pair.getSecond()));
				splits.remove(s);
				timePoints.get(tPos).remove(s.toSplit.data.label().get());

				LabelsImage li = provider.origin();
				int d = li.getAttribute(splitsZKey).orElse(0);
				li.setAttribute(splitsZKey,++d);

				this.fireAnnotationChange(splitted);
			}
			catch(OutOfLabelException e){
				log.warn("Cannot split the selected annotation", e);
				s.toSplit.setStatus(Status.DESELECTED);
				this.fireAnnotationChange(s.toSplit);
			}


		});




		// Amendements (timePoints.get(tPos) should now be empty)
		Set<SegmentedObject> changed = new HashSet<>();
		for(EditedAnnotation ea : new ArrayList<>(editions)){
			changed.addAll(provider.write(ea.toWrite.getRealFloat(), ea.iterator(), ea.sa.data.frame()));
			ea.delete();

			LabelsImage li = provider.origin();
			if(ea.isSpliter()){
				int d = li.getAttribute(splitsKey).orElse(0);
				li.setAttribute(splitsKey,++d);
			}else{
				int d = li.getAttribute(amendKey).orElse(0);
				li.setAttribute(amendKey,++d);
			}
		}

		for(PaintedAnnotation ea : new ArrayList<>(additions)){

			try{

				float label = provider.newLabel(tPos);
				changed.addAll(provider.write(label, ea.iterator(), ea.frame));			
				additions.remove(ea);

				LabelsImage li = provider.origin();
				int d = li.getAttribute(additionsKey).orElse(0);
				li.setAttribute(additionsKey,++d);

			}
			catch(OutOfLabelException e){
				log.warn("Cannot create a new object", e);
				this.removePaintInstance(ea.epi);
			}

		}

		 */
		this.fireAnnotationRedrawRequired(labelPlane);	

	}




	@Override
	public Stream<Annotation> annotations(long[] min, long[] max) {
		return provider.labels(min, max, tPos, zPos).map(f->{
			Annotation a = timePoints.get(tPos).get(f);
			if(a == null){
				final Optional<SegmentedObject> opt = provider.getItem(f, tPos);
				if(opt.isPresent()){
					a = new SegmentedAnnotation(opt.get());
					timePoints.get(tPos).put(f, (SegmentedAnnotation) a);
				}
				else
					a = decoy;
			}
			return a;
		});
	} 




	@SuppressWarnings("unchecked")
	@Override
	public Stream<Annotation> stream(String type) {
		final int t = tPos;
		Map<Object, AnnotationEdit<T, ?>> map = timePoints.get(t);
		if(map==null){
			map = new HashMap<>();
			timePoints.put(t, map);
		}
		final Map<Object, AnnotationEdit<T, ?>> fmap = map;
		return provider.labels(t).map(f->{
			Annotation a = fmap.get(f);
			if(a == null){
				final Optional<SegmentedObject> opt = provider.getItem(f, t);
				if(opt.isPresent()){
					a = new SegmentedAnnotation(opt.get());
					fmap.put(f, (SegmentedAnnotation) a);
				}
				else
					a = decoy;
			}
			return a;
		});
	}



	@Override
	public void produceCurrentPlaneIteration(AtomicBoolean stop, BiConsumer<long[], Annotation> consumer) {
		this.produceCurrentPlaneIteration(stop, consumer, maskPlane);
	}





	@SuppressWarnings("unchecked")
	@Override
	public void produceCurrentPlaneIteration(AtomicBoolean stop, BiConsumer<long[], Annotation> consumer, Interval itrv) {

		if(maskPlane == null || labelPlane == null)	return;

		final int timePos = tPos;
		final Map<Object, AnnotationEdit<T,?>> frame = timePoints.get(timePos);
		if(frame==null){
			//masks.clear();
			//timePoints.clear();
			//System.out.println("************************** Cache cleared **********************");
			return;
		}

		//System.out.println("SegmentedObjectManager producing plane for tPos : "+timePos);

		long[] dims = new long[itrv.numDimensions()];
		itrv.dimensions(dims);
		//System.out.println("Itrv -> "+Arrays.toString(dims));
		//System.out.println(maskPlane);

		final Cursor<ByteType> maskC = Views.interval(Views.extendZero(maskPlane), itrv).cursor();
		final RandomAccess<T> labelAccess = Views.interval(Views.extendZero(labelPlane), itrv).randomAccess();
		final long[] pos = new long[2];
		while(maskC.hasNext() && !stop.get()){
			ByteType bt = maskC.next();
			maskC.localize(pos);
			if(!annotationsVisible){
				consumer.accept(pos, null);
			}
			else if(bt.equals(MASK)){							
				labelAccess.setPosition(pos);
				final float f = labelAccess.get().getRealFloat();

				Annotation a = frame.get(f);
				if(a == null){
					final Optional<SegmentedObject> opt = provider.getItem(f, timePos);
					if(opt.isPresent()){
						a = new SegmentedAnnotation(opt.get());
						frame.put(f, (SegmentedAnnotation) a);
					}
				}
				consumer.accept(pos, a);
			}
			else if(!bt.equals(BG)){
				Annotation a = frame.get(bt);
				consumer.accept(pos, a);
			}
			else
				consumer.accept(pos, null);
		}
	}








	public static SegmentedObject createPrototype() {
		return new SegPrototype();
	}






	@Override
	public void save() throws IOException {
		provider.updateOnDisk();
	}




	@Override
	public MinimalImageInfo annotatedImageInfo() {
		return provider.origin().origin().getMinimalInfo().removeDimension(Image.c);
	}



	@Override
	public void positionHasChanged(ImageDisplay source, DisplayPosition dp) {

		synchronized(this){
			tPos = source.currentTimeFrame();
			zPos = source.currentZSlice();
		}

		modThread.requestRerun();		


		/*
		if(dp==DisplayPosition.FRAME){
			tPos = source.currentTimeFrame();
			// commit changes
			this.commitChanges();
			// Find the given EditorPaintInstance in added or edited
			editions.clear();
			additions.clear();
			try {
				this.save();
			} catch (IOException e) {
				UI.display("Error", "Unable to save changes to disk image", e, Level.SEVERE);
			}
			//Notify listener
			this.lstrs.forEach(l->l.annotationsReDrawRequired());
		}	
		 */

	}




	private boolean annotationsVisible = true;



	@Override
	public void toggleAnnotationsVisible() {
		annotationsVisible = !annotationsVisible;
		lstrs.forEach(l->l.annotationsReDrawRequired(maskPlane));
	}





	@Override
	public boolean annotationsAreVisible() {
		return  annotationsVisible;
	}








	@Override
	public void addAnnotationStatusListener(AnnotationStatusListener l) {
		statusLstrs.add(l);
	}








	@Override
	public void removeAnnotationStatusListener(AnnotationStatusListener l) {
		statusLstrs.remove(l);
	}











	private int lastT = tPos;
	private int maxSize = 10;


	private void checkMask() {
		//System.out.println("SegmentedResultEditor: checkingMask");

		final int t = tPos;
		RandomAccessibleInterval<ByteType> mask3D = this.masks.get(t);
		if(mask3D == null){
			synchronized(this){
				mask3D = io.createImg(provider.getFrame(0), new ByteType());
				if(masks.size()==maxSize){
					masks.remove(lastT);
					timePoints.remove(lastT);
				}
				masks.put(t, mask3D);
				timePoints.put(tPos, new HashMap<>());
			}
			rewriteMask(mask3D, false);
		}

		MinimalImageInfo info = annotatedImageInfo().removeDimension(Image.t);
		maskPlane = info.dimension(Image.z)==1 ? mask3D : Views.hyperSlice(mask3D, info.index(Image.z), zPos);
		labelPlane = provider.getSlice(t, zPos);
		this.lstrs.forEach(l->l.annotationsReDrawRequired(labelPlane));
	}



	private enum DisplayModes implements DisplayMode{
		PLAIN, CONTOUR
	}

	private DisplayMode dm = DisplayModes.CONTOUR;



	private void rewriteMask(Interval itrv, boolean erasePrevious){

		long b = System.currentTimeMillis();

		final int t = tPos;
		final RandomAccessibleInterval<ByteType> tMask = masks.get(t);
		if(tMask == null)
			return;

		final IntervalView<ByteType> mask = Views.interval(tMask, itrv);
		if(erasePrevious)
			mask.forEach(p->p.setZero());

		if(dm==DisplayModes.PLAIN){

			final Cursor<T> labelCursor = Views.interval(provider.getFrame(t),itrv).cursor();
			final RandomAccess<ByteType> maskAccess = mask.randomAccess();

			while(labelCursor.hasNext()){
				T label = labelCursor.next();
				if(label.getRealFloat()!=0f){
					maskAccess.setPosition(labelCursor);
					maskAccess.get().set(MASK);
				}
			}

		}
		else{			

			long[] min = new long[itrv.numDimensions()]; 	itrv.min(min);
			long[] max = new long[itrv.numDimensions()]; 	itrv.max(max);

			System.out.println("Rewriting mask for itrv : "+Arrays.toString(min)+" ; "+Arrays.toString(max));
			final OutOfBoundsMirrorFactory<T,RandomAccessibleInterval<T>> fctry = new OutOfBoundsMirrorFactory<>( OutOfBoundsMirrorFactory.Boundary.SINGLE );
			final RandomAccess<ByteType> maskAccess = Views.extend(mask, new OutOfBoundsConstantValueFactory<>(BG)).randomAccess();			
			Outline.run( Views.interval(Views.extend(provider.getFrame(t), fctry) , Intervals.expand(itrv, 1)), 
					//final RandomAccess<ByteType> maskAccess = mask.randomAccess();			
					//Outline.run(Views.interval(provider.getFrame(tPos), itrv),  
					l->{						
						maskAccess.setPosition(l);
						maskAccess.get().set(MASK);
					},
					fctry);

		}


		System.out.println("SegmentedObjectEditor: rewrite of frame "+t+" took "+(System.currentTimeMillis()-b)+"ms");



	}










	private class AnnotationDecoy implements Annotation{

		public AnnotationDecoy() {
			System.out.println("new decoy from "+getClass().getSimpleName());
		}


		@Override
		public AnnotationStatus status() {
			return AnnotationStatus.DESELECTED;
		}




		@Override
		public Stream<AKey<?>> properties() {
			return Stream.empty();
		}


		@Override
		public <E> E getProperty(AKey<E> k) {
			return null;
		}


		@Override
		public String representedType() {
			return "NONE";
		}


	}






	private static class SegPrototype extends DataNode implements SegmentedPrototype{

		@Override
		public String typeId() {
			return getClass().getSimpleName();
		}

		@Override
		public long[] location() {
			return getAttribute(Keys.location).get();
		}

		@Override
		public double[] centroid() {
			return getAttribute(Keys.centroid).get();
		}

		@Override
		public Stream<AKey<?>> minimal() {
			return null;
		}

		@Override
		public Optional<LabelsImage> origin() {
			return Optional.empty();
		}

		@Override
		public SegmentedPrototype create(float label) {
			SegPrototype sp = new SegPrototype();
			sp.setAttribute(labelKey, label);
			return sp;
		}

		public String toString(){
			return "Shape with label "+getAttribute(labelKey).get();
		}

	}






	private interface AnnotationEdit<T,DATA> extends PaintableAnnotation<ByteType,T>{
		//public void setStatus(AnnotationStatus selected);
		public int frame();
		public DATA data();
		public void validate();
		public boolean undo();
		public void toggleSelected();
		public boolean setDeleted(boolean b);
		public void updateBoundingBox(BoundingBox bb);
	}





	private interface AnnotationContainer<T,DATA> extends AnnotationEdit<T,DATA>{
		public void remove(Annotation sa);
	}






	private class SegmentedAnnotation implements AnnotationEdit<T,SegmentedObject>{

		private AnnotationStatus currentStatus = AnnotationStatus.DESELECTED;
		private AnnotationStatus deselectedStatus = AnnotationStatus.DESELECTED;
		private SegmentedObject data;		
		private AnnotationContainer<T,?> ma;

		public SegmentedAnnotation(SegmentedObject id) {
			this.data = id;
			//System.out.println("new "+this.toString());
		}


		public SegmentedObject data(){
			return data;
		}

		@Override
		public int frame() {
			return data.frame();
		}



		public void setStatus(AnnotationStatus newStatus){
			currentStatus = newStatus;
			if(ma != null && currentStatus != newStatus){				
				ma.remove(this);
				ma = null;
			}
		}

		@Override
		public AnnotationStatus status() {
			return currentStatus;
		}



		void setMerged(MergedAnnotation ma){
			currentStatus = Status.MERGED;
			this.ma = ma;
		}




		@Override
		public PaintInstanceConsumer<ByteType,T> getHandler() {
			return SegmentedObjectManager.this;
		}




		@Override
		public void validate() {
			if(currentStatus==Status.DELETED){
				//Set status and remove from cache
				timePoints.get(frame()).remove(data().label().get());
				currentStatus = Status.EFFECTIVELY_DELETED;
				//erase from mask
				final RandomAccess<ByteType> access = masks.get(tPos).randomAccess();
				provider.processPoints(data.label().get(), data.frame(), l->{
					access.localize(l);
					access.get().set(BG);
				}); 
				//delete from provider
				provider.delete(data);
			}
		}


		@Override
		public boolean undo() {
			if(currentStatus == Status.DELETED){
				currentStatus = Status.DESELECTED;
				deselectedStatus = Status.DESELECTED;
				return true;
			}
			else if(currentStatus == Status.SELECTED){
				currentStatus = Status.DESELECTED;
				return true;
			}
			return false;
		}


		@Override
		public void toggleSelected() {
			if(currentStatus==Status.SELECTED)
				currentStatus = deselectedStatus;
			else 
				currentStatus = Status.SELECTED;
			statusLstrs.forEach(l->l.statusChanged(this));
		}


		@Override
		public boolean setDeleted(boolean b) {
			if(currentStatus==Status.EFFECTIVELY_DELETED)
				return false;
			if(b && currentStatus != Status.DELETED){
				currentStatus = Status.DELETED;
				deselectedStatus = Status.DELETED;
				return true;
			}
			else if(!b && currentStatus==Status.DELETED){
				currentStatus = Status.DESELECTED;
				deselectedStatus = Status.DESELECTED;
				return true;
			}
			return false;
		}



		@SuppressWarnings("unchecked")
		@Override
		public boolean equals(Object o){
			if(o==null)return false;
			if(SegmentedAnnotation.class.isAssignableFrom(o.getClass()))
				return data.label().get().equals(((SegmentedAnnotation) o).data.label().get());
			else return false;
		}

		@Override
		public int hashCode(){
			return Float.hashCode(data.label().get());
		}


		@Override
		public String toString(){
			return data.toString();
		}


		@Override
		public void updateBoundingBox(BoundingBox bb) {
			bb.update(data.getAttribute(Keys.bbMin).get());
			bb.update(data.getAttribute(Keys.bbMax).get());
		}


		@Override
		public Stream<AKey<?>> properties() {
			return data.getValidAttributeKeys();
		}


		@Override
		public <E> E getProperty(AKey<E> k) {
			return data.getAttribute(k).get();
		}

		@Override
		public String representedType() {
			return data.declaredType();
		}



	}







	private class PaintedAnnotation implements AnnotationEdit<T,Float>, PaintInstanceListener{

		private PaintInstance<ByteType,T> epi;
		private final int frame;
		private final AnnotationStatus status = Status.ADDED;
		private final float data;

		public PaintedAnnotation(int frame, float data) {
			this.frame = frame;
			this.data = data;
			additions.add(this);
			timePoints.get(tPos).put(NOVEL, this);
		}

		public void setPaintInstance(PaintInstance<ByteType,T> epi){
			this.epi = epi;
			epi.addPaintInstanceListener(this);
		}




		@Override
		public int frame() {
			return frame;
		}



		@Override
		public AnnotationStatus status() {
			return status;
		}



		@Override
		public String toString(){
			return "Painted "+epi.getAnnotation().toString();
		}


		@Override
		public PaintInstanceConsumer<ByteType, T> getHandler() {
			return SegmentedObjectManager.this;
		}


		@Override
		public Float data() {
			return data;
		}

		@Override
		public void validate() {

			// Paint on provided image
			// Ensure the is contained by mask
			final Interval itrv = Intervals.intersect(masks.get(tPos), new FinalInterval(epi.min(),epi.max()));

			final Cursor<ByteType> cursor = Views.interval(masks.get(tPos), itrv).cursor();
			final long[] pos = new long[cursor.numDimensions()];
			final Iterator<long[]> it = new Iterator<long[]>(){

				@Override
				public boolean hasNext() {
					while(cursor.hasNext()){
						if(cursor.next().equals(NOVEL)){
							cursor.localize(pos);
							return true;
						}
					}
					return false;
				}

				@Override
				public long[] next() {
					return pos;
				}

			};

			final List<SegmentedObject> amended = provider.write(data, it, tPos);
			//Should only be one
			System.out.println("Amended size = "+amended.size());
			//Remove this from cache
			additions.remove(this);
			timePoints.get(tPos).remove(NOVEL);
			//Add new annotation
			timePoints.get(tPos).put(amended.get(0).label().get(),new SegmentedAnnotation(amended.get(0)));
			//Deregister
			epi.removePaintInstanceListener(this);

			final BoundingBox bb = new BoundingBox(2);
			bb.update(epi.min());	
			bb.update(epi.max());	

			//Rebuild mask
			rewriteMask(itrv, true);

			lstrs.forEach(l->l.annotationsReDrawRequired(bb));

		}

		@Override
		public boolean undo() {
			epi.clear();
			epi.removePaintInstanceListener(this);
			additions.remove(this);
			timePoints.get(tPos).remove(data);
			return true;
		}

		@Override
		public void toggleSelected() {}


		@Override
		public void updateBoundingBox(BoundingBox bb) {
			bb.update(epi.min());
			bb.update(epi.max());
		}

		@Override
		public boolean setDeleted(boolean b) {
			return false;
		}


		@Override
		public void paintJobPerformed(PaintInstance<?, ?> source, long[] min, long[] max) {
			//System.out.println("PaintJob done");
			final BoundingBox bb = new BoundingBox(2);
			bb.update(source.min());
			bb.update(source.max());
			lstrs.forEach(l->l.annotationsReDrawRequired(bb));
		}


		@Override
		public void cancelled(PaintInstance<?, ?> source) {
			final BoundingBox bb = new BoundingBox(2);
			bb.update(source.min());
			bb.update(source.max());
			lstrs.forEach(l->l.annotationsReDrawRequired(bb));
			epi.removePaintInstanceListener(this);
		}

		@Override
		public Stream<AKey<?>> properties() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <E> E getProperty(AKey<E> k) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String representedType() {
			// TODO Auto-generated method stub
			return null;
		}


	}





	private class EditedAnnotation implements AnnotationEdit<T, SegmentedAnnotation>, PaintInstanceListener{

		private final PaintInstance<ByteType,T> epi;
		private final SegmentedAnnotation sa;
		private final ByteType toWrite;
		private final AnnotationStatus status;



		public EditedAnnotation(PaintInstance<ByteType,T> epi, ByteType toWrite) {
			this.epi = epi;
			epi.addPaintInstanceListener(this);
			this.sa = (SegmentedAnnotation) epi.getAnnotation();
			sa.setStatus(Status.AMENDED);
			this.toWrite = toWrite;
			timePoints.get(tPos).put(toWrite, this);
			if(toWrite==ERASE)
				status = Status.ERASED;
			else if(toWrite == EXPAND)
				status = Status.EXPANDED;
			else if(toWrite == SPLIT)
				status = Status.SPLITTED;
			else
				throw new RuntimeException("Cannot interpret value to be written : "+toWrite);
		}




		@Override
		public String toString(){
			return "Edited Annotation " + sa.data().label().get() + " rewrite with "+toWrite;
		}


		@Override
		public PaintInstanceConsumer<ByteType, T> getHandler() {
			return SegmentedObjectManager.this;
		}



		@Override
		public AnnotationStatus status() {
			return status;
		}


		@Override
		public int frame() {
			return sa.frame();
		}


		@Override
		public SegmentedAnnotation data() {
			return sa;
		}


		@Override
		public void validate() {

			//Deregister
			epi.removePaintInstanceListener(this);

			//new Thread(()->{

			//System.out.println("New thread from Edited Annotation validating its work");
			// paint on provided image
			// the value to write depends on out toWrite ByteType
			float value = -1;
			if(toWrite==ERASE)
				value = 0f;
			else if(toWrite == EXPAND){
				value = sa.data().label().get();
			}
			else if(toWrite == SPLIT)
				try {
					value = provider.newLabel(tPos);
				} catch (OutOfLabelException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			else
				throw new RuntimeException("Cannot interpret value to be written : "+toWrite);

			//Make sure the interval from the paint instance is within our mask bounds
			final long[] fMin = new long[masks.get(tPos).numDimensions()];
			final long[] fMax = new long[masks.get(tPos).numDimensions()];
			for(int i = 0; i<fMin.length; i++){
				fMin[i] = FastMath.max(fMin[i], epi.min()[i]);
				fMax[i] = FastMath.min(masks.get(tPos).dimension(i)-1, epi.max()[i]);
			}

			final Cursor<ByteType> cursor = Views.interval(masks.get(tPos),new FinalInterval(fMin,fMax)).cursor();
			final long[] pos = new long[cursor.numDimensions()];
			final Iterator<long[]> it = new Iterator<long[]>(){

				@Override
				public boolean hasNext() {
					while(cursor.hasNext()){
						if(cursor.next().equals(toWrite)){
							cursor.localize(pos);
							return true;
						}
					}
					return false;
				}

				@Override
				public long[] next() {
					return pos;
				}

			};

			final List<SegmentedObject> amended = provider.write(value, it, tPos);

			//Should be either one or two
			System.out.println("Amended size = "+amended.size());
			//Remove this from cache
			editions.remove(this);
			timePoints.get(tPos).remove(toWrite);

			//Add new annotation (should only happen when Split)
			//Also keep track of the region to be redrawn
			final BoundingBox bb = new BoundingBox(2);
			final BoundingBox bb3D = new BoundingBox(amended.get(0).centroid().length);
			for(SegmentedObject so : amended){
				long[] min = so.getAttribute(Keys.bbMin).get();
				bb.update(min); bb3D.update(min);
				long[] max = so.getAttribute(Keys.bbMax).get();
				bb.update(max); bb3D.update(max);
				@SuppressWarnings("unchecked")
				SegmentedAnnotation sa = (SegmentedAnnotation) timePoints.get(tPos).get(so.label().get());
				if(sa==null){					
					sa = new SegmentedAnnotation(so);
					timePoints.get(tPos).put(so.label().get(),sa);
				}
				sa.setStatus(Status.DESELECTED);
			}



			//repaint Mask (We need the bounding box in the dimension of the mask3d)
			rewriteMask(bb3D, true);			
			lstrs.forEach(l->l.annotationsReDrawRequired(bb));

			System.out.println("Validated EditAnnotation done!");

			//}).start();


		}




		@Override
		public boolean undo() {
			final BoundingBox bb = new BoundingBox(2);
			bb.update(epi.min()); 	bb.update(epi.max());
			this.epi.clear();
			editions.remove(this);
			timePoints.get(tPos).remove(toWrite);
			//repaint sa
			final RandomAccess<ByteType> access = masks.get(tPos).randomAccess();
			provider.processPoints(sa.data().label().get(), tPos, l->{
				access.localize(l);
				access.get().set(MASK);
			});
			bb.update(sa.data().getAttribute(Keys.bbMin).get());
			bb.update(sa.data().getAttribute(Keys.bbMax).get());
			lstrs.forEach(c->c.annotationsReDrawRequired(bb));
			return true;
		}


		@Override
		public void toggleSelected() {}


		@Override
		public boolean setDeleted(boolean deleted) {
			return false;
		}


		@Override
		public void updateBoundingBox(BoundingBox bb) {
			bb.update(epi.min());
			bb.update(epi.max());
		}


		@Override
		public void paintJobPerformed(PaintInstance<?, ?> source, long[] min, long[] max) {
			//System.out.println("PaintJob done");
			final BoundingBox bb = new BoundingBox(2);
			bb.update(source.min());
			bb.update(source.max());
			lstrs.forEach(l->l.annotationsReDrawRequired(bb));
		}


		@Override
		public void cancelled(PaintInstance<?, ?> source) {
			final BoundingBox bb = new BoundingBox(2);
			bb.update(source.min());
			bb.update(source.max());
			lstrs.forEach(l->l.annotationsReDrawRequired(bb));
			epi.removePaintInstanceListener(this);
		}




		@Override
		public Stream<AKey<?>> properties() {
			// TODO Auto-generated method stub
			return null;
		}




		@Override
		public <E> E getProperty(AKey<E> k) {
			// TODO Auto-generated method stub
			return null;
		}


		@Override
		public String representedType() {
			// TODO Auto-generated method stub
			return null;
		}


	}






	private class MergedAnnotation implements AnnotationContainer<T,Set<SegmentedAnnotation>>{

		private final Set<SegmentedAnnotation>  toMerge;
		private final float id;
		private AnnotationStatus status = Status.MERGED;

		public MergedAnnotation(Set<SegmentedAnnotation> merged, float f) {
			this.toMerge = merged;
			toMerge.forEach(sa->sa.setMerged(this));
			this.id = f;
			timePoints.get(tPos).keySet().removeAll(toMerge);
			timePoints.get(tPos).put(id(), this);	
			merges.add(this);
		}


		public Interval getBoundingBox() {
			final BoundingBox bb = new BoundingBox(2);
			for(SegmentedAnnotation sa : toMerge)
				sa.updateBoundingBox(bb);
			return bb;
		}


		public void remove(Annotation a) {
			toMerge.remove(a);
			if(toMerge.isEmpty())
				merges.remove(this);
		}



		@Override
		public String toString(){
			return "Merged Annotation ";
		}


		@Override
		public AnnotationStatus status() {
			return status;
		}


		@Override
		public int frame() {
			return toMerge.iterator().next().data().frame();
		}


		@Override
		public PaintInstanceConsumer<ByteType, T> getHandler() {
			return SegmentedObjectManager.this;
		}


		@Override
		public Set<SegmentedAnnotation> data() {
			return toMerge;
		}


		public float id(){
			return id;
		}


		@Override
		public void validate() {
			final SegmentedObject so = provider.merge(toMerge.stream().map(sa->sa.data()).collect(Collectors.toList()));
			timePoints.get(tPos).put(so.label().get(), new SegmentedAnnotation(so));
			timePoints.get(tPos).remove(id());
			final BoundingBox bb = new BoundingBox(annotatedImageInfo().numDimensions());
			updateBoundingBox(bb);
			rewriteMask(bb, true);
		}


		@Override
		public boolean undo() {
			timePoints.get(tPos).remove(id());
			for(SegmentedAnnotation sa : toMerge)
				timePoints.get(tPos).put(sa.data().label().get(), sa);
			merges.add(this);
			return true;
		}


		@Override
		public void toggleSelected() {
			if(status==Status.MERGED)
				status = AnnotationStatus.SELECTED;
			else
				status = Status.MERGED;
		}


		@Override
		public boolean setDeleted(boolean deleted) {
			return false;
		}


		@Override
		public void updateBoundingBox(BoundingBox bb) {
			for(SegmentedAnnotation sa : toMerge)
				sa.updateBoundingBox(bb);
		}


		@Override
		public Stream<AKey<?>> properties() {
			return Stream.of(AKey.get("Merged", String.class));
		}


		@Override
		public <E> E getProperty(AKey<E> k) {
			return (E)toString();
		}

		@Override
		public String representedType() {
			// TODO Auto-generated method stub
			return null;
		}


	}





	private class SplitAnnotation implements AnnotationEdit<T,SegmentedAnnotation>{

		private final SegmentedAnnotation  toSplit;
		private final long slice;
		private AnnotationStatus status = Status.SPLIT;


		public SplitAnnotation(SegmentedAnnotation sa, long slice) {
			this.toSplit = sa;
			sa.setStatus(Status.SPLIT);
			this.slice = slice;
			timePoints.get(tPos).put(sa.data().label().get(), this);
		}



		@Override
		public String toString(){
			return toSplit+" cut at slice "+slice;
		}


		@Override
		public PaintInstanceConsumer<ByteType, T> getHandler() {
			return SegmentedObjectManager.this;
		}


		@Override
		public AnnotationStatus status() {
			return status;
		}



		@Override
		public int frame() {
			return toSplit.data().frame();
		}


		@Override
		public SegmentedAnnotation data() {
			return toSplit;
		}


		@Override
		public void validate() {
			try {

				final Pair<SegmentedObject,SegmentedObject> split = provider.splitZ(toSplit.data(), slice);
				timePoints.get(tPos).remove(data().data().label().get());
				timePoints.get(tPos).put(split.getFirst().label().get(), new SegmentedAnnotation(split.getFirst()));
				timePoints.get(tPos).put(split.getSecond().label().get(), new SegmentedAnnotation(split.getSecond()));


				// Now redraw
				final BoundingBox bb = new BoundingBox(annotatedImageInfo().numDimensions());
				toSplit.updateBoundingBox(bb);
				rewriteMask(bb, true);


			} catch (OutOfLabelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}


		@Override
		public boolean undo() {
			timePoints.get(tPos).put(toSplit.data().label().get(), toSplit);
			splits.remove(this);
			return true;
		}


		@Override
		public void toggleSelected() {
			if(status==Status.SPLIT)
				status = AnnotationStatus.SELECTED;
			else
				status = Status.SPLIT;
			toSplit.setStatus(status);
		}


		@Override
		public boolean setDeleted(boolean deleted) {
			return false;
		}


		@Override
		public void updateBoundingBox(BoundingBox bb) {
			toSplit.updateBoundingBox(bb);
		}



		@Override
		public Stream<AKey<?>> properties() {
			return Stream.of(AKey.get("Split", String.class));
		}


		@Override
		public <E> E getProperty(AKey<E> k) {
			return (E)toString();
		}

		@Override
		public String representedType() {
			// TODO Auto-generated method stub
			return null;
		}


	}




}
