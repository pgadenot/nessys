package org.pickcellslab.nessys.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.distribution.BetaDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.distribution.WeibullDistribution;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

/**
 * 
 * Implements a <a href="https://en.wikipedia.org/wiki/Naive_Bayes_classifier">Naive Bayes Classifier</a>.
 * 
 * @author Guillaume Blin
 *
 */
public class NaiveBayesClassifier {

	public static final String UNKNOWN = "Unknown";

	// name of the classifier
	private String name;
	// The File where the training set is saved
	private File file;

	// The classes known to this classifier
	private final String[] classes;
	// The priors of the classifier
	private final double[] priors;
	// Statistical distributions holding the model [classes][features]
	private final RealDistribution[][] distris;
	
	// A Mapping of the classes names and their indices in distris
	private final Map<String,Integer> indices = new HashMap<>();


	private NaiveBayesClassifier(String[] classes, double[] priors, RealDistribution[][] distris) {
		this.classes = classes;
		this.priors = priors;
		this.distris = distris;
		for(int i = 0; i<classes.length; i++)
			indices.put(classes[i], i);
	}


	/**
	 * @param c The class for which to compute the posterior probability
	 * @param value The feature vector for which the posterior probability is desired
	 * @return The posterior probability for the given feature vector to belong to the given class
	 */
	public double getPosterior(String c, double[] value){
		return getPosterior(indices.get(c), value);		
	}


	/**
	 * Generates a List of feature vectors for a given class
	 * @param classe The desired class
	 * @param n The number of vectors to be generated
	 * @return A {@link List} of feature vector for the desired object class of size n
	 */
	public List<double[]> random(String classe, int n){
		int c = indices.get(classe);
		List<double[]> list = new ArrayList<>();
		for(int i = 0;i<n; i++){
			double[] v = new double[distris[c].length];
			for(int d = 0; d<distris[c].length; d++){
				v[d] = distris[c][d].sample();
			}
			list.add(v);
		}
		return list;
	}


	/**
	 * @param value A feature vector for which the classification is needed
	 * @return The name of the predicted class
	 */
	public String getPrediction(double[] value){

		double maxPost = Double.NEGATIVE_INFINITY;
		int max = -1;
		for(int i = 0; i<classes.length; i++){
			double p = getPosterior(i, value);

			if(p>maxPost){
				maxPost = p;
				max = i;
			}
		}
		if(max==-1 || maxPost == 0d)
			return UNKNOWN;			
		else
			return classes[max];
	}


	private double getPosterior(int i, double[] value){
		RealDistribution[] ds = distris[i];
		double post = priors[i];
		for(int d = 0; d<ds.length; d++){
			double prob = ds[d].density(value[d]);
			//	System.out.println(prob);
			post *= prob;
		}
		//System.out.println("Posterior = "+post);

		return post;
	}



	/**
	 * @return The {@link File} where the training set is saved
	 */
	public File getFile() {
		return file;
	}



	/**
	 * Sets the {@link File} where the training set of this classifier is saved
	 * @param file
	 */
	public void setFile(File file){
		this.file = file;
		this.name = file.getName();
	}



	@Override
	public String toString(){
		return name == null ? super.toString() : name;
	}






	/**
	 * A Builder of {@link NaiveBayesClassifier}
	 *
	 */
	public static class ClassifierBuilder{

		private final Map<String, List<double[]>> trainingSet; 

		private int length = -1;

		private String[] classes;

		private File file;

		/**
		 * Initialises the building process
		 * @param classes A String array with the name of possible classes
		 */
		public ClassifierBuilder(String[] classes) {
			this.classes = Arrays.copyOf(classes, classes.length);
			this.trainingSet = new HashMap<>();
			for(String s : classes)
				trainingSet.put(s, new ArrayList<>());
		}



		/**
		 * Adds a new data point in the training set
		 * @param set The name of the class the data belongs to
		 * @param value The feature vector
		 */
		public void addTrainingValue(String set, double[] value){

			if(length!=-1)
				assert value.length == length : "The provided value length is not consistent with previously entered values: "+value.length;
			else
				length = value.length;

			assert trainingSet.containsKey(set) : "Unknown class : "+set;

			trainingSet.get(set).add(value);

		}


		/**
		 * Removes a data point from the training set
		 * @param set The name of the class the data belongs to
		 * @param value The feature vector.
		 */
		public void removeTrainingValue(String set, double[] value){
			assert trainingSet.containsKey(set) : "Unknown class : "+set;
			Iterator<double[]> list = trainingSet.get(set).iterator();
			while(list.hasNext()){
				double[] n = list.next();
				if(Arrays.equals(n, value)){
					list.remove();
					System.out.println("Value removed from "+set);
					break;
				}
			}
		}



		/**
		 * @param priors prior values used to initialise the classifier (the length of the array must be equal to the number of classes)
		 * @return a new (Gaussian) {@link NaiveBayesClassifier} initialised with the given priors.
		 */
		public NaiveBayesClassifier build(double[] priors){
			RealDistribution[][] distris = new RealDistribution[classes.length][length];
			for(int i = 0; i<classes.length; i++){
				List<double[]> list = trainingSet.get(classes[i]);

				for(int j = 0; j<length; j++){					
					// compute mean and sigma
					SummaryStatistics stats = new SummaryStatistics();
					for(double[] array : list)
						stats.addValue(array[j]);


					distris[i][j] = new NormalDistribution(stats.getMean(), stats.getStandardDeviation());
					//	System.out.println(distris[i][j]);
				}
			}		
			NaiveBayesClassifier nbc = new NaiveBayesClassifier(classes, priors, distris);
			if(file!=null)
				nbc.setFile(file);
			return nbc;
		}


/*

		public NaiveBayesClassifier build(double[] priors){

			RealDistribution[][] distris = new RealDistribution[classes.length][length];

			for(int i = 0; i<classes.length; i++){

				//System.out.println("Class : "+classes[i]);

				for(int j = 0; j<length; j++){	

					//System.out.println("Param : "+j);

					List<double[]> list = trainingSet.get(classes[i]);
					double[] arr = new double[list.size()];
					//DescriptiveStatistics stats = new DescriptiveStatistics();
					for(int s = 0; s<list.size(); s++){					
						arr[s] = list.get(s)[j];
						//	stats.addValue(arr[s]);
					}

					//	System.out.println(stats.getSkewness());
					//	if(stats.getSkewness()<-0.2 || stats.getSkewness()>0.2){
					int bin = (int) Math.max(Math.sqrt(arr.length), 3);
					EmpiricalDistribution d = new KdeDistribution(bin);
					distris[i][j] = d;
					d.load(arr);
					//System.out.println("Kde chosen");
					//	}
					//	else{
					//		distris[i][j] = new NormalDistribution(null, stats.getMean(), stats.getStandardDeviation());
					//		System.out.println("Normal chosen");
					//	}
				}

			}		
			return new NaiveBayesClassifier(classes, priors, distris);
		}

*/



		/**
		 * Saves the training set of this classifier to the file system (as a tab separated value)
		 * @param file The {@link File} where the classifier should be saved
		 */
		public void saveToFile(File file){

			try {

				PrintWriter pw = new PrintWriter(file);
				//Header
				pw.print("Class");
				for(String s : Ridge.getFeatureNames()){
					pw.print("\t"+s);
				}


				trainingSet.forEach((classe, list)->{

					for(double[] arr : list){
						pw.println("");
						pw.print(classe);
						for(double d : arr)
							pw.print("\t"+d);
					}
				});

				pw.flush();
				pw.close();

				this.file = file;

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}



		}




		/**
		 * Builds a {@link NaiveBayesClassifier} from a training set located on the file system
		 * @param file The {@link File} where the classifier should be saved
		 * @return A new {@link NaiveBayesClassifier} built from the training set loaded from file.
		 * @see ClassifierBuilder#saveToFile
		 */
		public static NaiveBayesClassifier fromFile(File file){		

			ClassifierBuilder cBuilder = ClassifierBuilder.builderFromFile(file);
			NaiveBayesClassifier classifier = cBuilder.build(new double[]{0.5,0.5});
			classifier.setFile(file);
			return classifier;

		}




		/**
		 * @return true if the classifier has enough training data, false otherwise.
		 */
		public boolean hasTrainingSet() {
			for(String s : classes)
				if(trainingSet.get(s).size()<10)
					return false;
			return true;
		}


		/**
		 * @param classe The desired class of objects
		 * @return The current number of training data points for the given class
		 */
		public int getCurrentTrainingSetSize(String classe){
			final List<double[]> set = trainingSet.get(classe);
			return set == null ? 0 : set.size();
		}
		


		/**
		 * Generates a new {@link ClassifierBuilder} from an existing {@link NaiveBayesClassifier}. This is useful 
		 * to be able to reuse an existing classifier's dataset and to edit it.
		 * @param nbc A {@link NaiveBayesClassifier} instance for which a new builder needs to be created
		 * @return A new {@link ClassifierBuilder} instance containing a new (editable) copy of the input's training set
		 */
		public static ClassifierBuilder builderFromClassifier(NaiveBayesClassifier nbc) {
			assert nbc != null : "Classifier is null";			
			return ClassifierBuilder.builderFromFile(nbc.file);
		}



		private static ClassifierBuilder builderFromFile(File file) {

			String line = "";
			String cvsSplitBy = "\t";

			ClassifierBuilder cBuilder = new ClassifierBuilder(RidgeSegmentationOptions.classes);

			try (BufferedReader br = new BufferedReader(new FileReader(file))) {

				//headers
				br.readLine();

				while ((line = br.readLine()) != null) {

					// use comma as separator
					String[] row = line.split(cvsSplitBy);
					double[] value = new double[row.length-1];
					for(int i = 1; i<row.length; i++)
						value[i-1] = Double.parseDouble(row[i]);

					cBuilder.addTrainingValue(row[0].replace("\"", ""), value);

				}

				return cBuilder;

			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}




		/**
		 * Same as {@link #fromFile(File)} but from a {@link BufferedReader}
		 * @param br The {@link BufferedReader} to read from
		 * @param name A name for the new classifier
		 * @return A new {@link NaiveBayesClassifier} built from the resource.
		 */
		public static NaiveBayesClassifier fromResource(BufferedReader br, String name) {



			String line = "";
			String cvsSplitBy = "\t";

			ClassifierBuilder cBuilder = new ClassifierBuilder(RidgeSegmentationOptions.classes);

			try {

				//headers
				br.readLine();

				while ((line = br.readLine()) != null) {

					// use comma as separator
					String[] row = line.split(cvsSplitBy);
					double[] value = new double[row.length-1];
					for(int i = 1; i<row.length; i++)
						value[i-1] = Double.parseDouble(row[i]);

					cBuilder.addTrainingValue(row[0], value);

				}

				NaiveBayesClassifier nbc = cBuilder.build(new double[]{0.5,0.5});
				nbc.name = name;

				return nbc;

			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}




	}



	public static void main(String[] args) {

		RealDistribution invalid1 = new BetaDistribution(2, 2);
		RealDistribution valid1 = new BetaDistribution(0.5, 0.5);
		RealDistribution invalid2 = new WeibullDistribution(0.5, 1);
		RealDistribution valid2 = new WeibullDistribution(1.5, 1);
		RealDistribution invalid3 = new NormalDistribution(1000, 500);
		RealDistribution valid3 = new NormalDistribution(1250,500);

		ClassifierBuilder cBuilder = new ClassifierBuilder(RidgeSegmentationOptions.classes);


		for(int i = 0; i<30; i++){
			double[] v = new double[3];
			v[0] = valid1.sample();
			v[1] = valid2.sample();
			v[2] = valid3.sample();	
			cBuilder.addTrainingValue(RidgeSegmentationOptions.classes[0], v);
			double[] w = new double[3];
			w[0] = invalid1.sample();
			w[1] = invalid2.sample();
			w[2] = invalid3.sample();
			cBuilder.addTrainingValue(RidgeSegmentationOptions.classes[1], w);
		}


		NaiveBayesClassifier classifier = cBuilder.build(new double[]{0.5, 0.5});

		int valid = 0, invalid = 0, unknown = 0;
		for(int i = 0; i<1000; i++){

			double[] v = new double[3];
			v[0] = valid1.sample();
			v[1] = valid2.sample();
			v[2] = valid3.sample();

			double[] w = new double[3];
			w[0] = invalid1.sample();
			w[1] = invalid2.sample();
			w[2] = invalid3.sample();

			String correct = classifier.getPrediction(v);
			String wrong = classifier.getPrediction(w);
			if(correct == RidgeSegmentationOptions.classes[0])
				valid++;
			else if(correct == UNKNOWN)
				unknown++;
			if(wrong == RidgeSegmentationOptions.classes[1])
				invalid++;
			else if(wrong == UNKNOWN)
				unknown++;
		}



		System.out.println("Valid accuracy "+valid);
		System.out.println("Invalid accuracy "+invalid);
		System.out.println("Number of unknown "+unknown);
	}







}
