package org.pickcellslab.nessys.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * A {@link PopulationInfoCollector} accepts a series of objects (the population) to eventually produce a result.
 * <br> For example a {@link PopulationInfoCollector} could accept a collection of segmented areas and return summary statistics on the eccentricity of the area. 
 *
 * @param <V> The type of object accepted by this {@link PopulationInfoCollector}
 * @param <O> The type of result returned by this {@link PopulationInfoCollector}
 */
public interface PopulationInfoCollector<V, O> {

	/**
	 * Includes the given object into the final result
	 * @param v The object to include
	 */
	public void accept(V v);
	
	/**
	 * @return The result of the info collection including all the objects previously passed to {@link #accept(Object)}.
	 * Please note that a {@link PopulationInfoCollector} is likely to be stateful, future calls to this method may return
	 * a distinct result if in the meantime other objects have been passed to {@link #accept(Object)}.
	 */
	public O deliver();
	
}
