package org.pickcellslab.nessys.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.DoubleAdder;
import java.util.stream.Stream;

import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedPrototype;
import org.pickcellslab.pickcells.api.img.process.Outline;

import net.imglib2.Cursor;
import net.imglib2.FinalInterval;
import net.imglib2.Interval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;

/**
 *
 * A {@link RidgeArea} is a {@link SegmentedPrototype} representing a region with a unique label within a 2D segmented image.
 * 
 *
 * @param <T> The {@link Type} of the segmented image where this {@link RidgeArea} is defined
 */
public class RidgeArea<T extends RealType<T> & NativeType<T>> extends DataNode implements SegmentedPrototype {

	
	enum Ambiguity{
		NONE, UP, DOWN
	}
	
	
	static AKey<Integer> slice = AKey.get("Slice",Integer.class);
	private static AKey<Float> newLabelKey = AKey.get("Relabel", Float.class);

	private int share = 0;
	private Ambiguity ambiguity = Ambiguity.NONE;
	/**
	 * The color used during the graph colouring phase
	 */
	private T color = null;

	protected static final List<AKey<?>> keys = new ArrayList<>(6);

	static{	
		keys.add(idKey);
		keys.add(labelKey);
		keys.add(centroid);
		keys.add(Keys.bbMin);
		keys.add(Keys.bbMax);
		keys.add(Keys.location);
		keys.add(slice);
	}


	
	
	void setAmbiguity(Ambiguity a){
		this.ambiguity = a;
	}
	
	
	Ambiguity getAmbiguity(){
		return ambiguity;
	}
		
	boolean isAmbiguous(){
		return ambiguity != Ambiguity.NONE;
	}

	
	
	/**
	 * Sets the color of this {@link RidgeArea} for the purpose of graph coloring
	 * @param color The color to set
	 */
	void setColor(T color){
		this.color = color;
	}
	
	/**
	 * @return The colour of this {@link RidgeArea} used for the purpose of graph coloring (may be null)
	 */
	T getColor(){
		return color;
	}
	
	/**
	 * @return true if this {@link RidgeArea} is already associated with a colour (graph coloring), false otherwise
	 */
	boolean hasColor(){
		return color != null;
	}
	
	
	public int numberOfTrees(){
		return share;
	}

	public void registerTree(){
		share++;
	}

	public boolean isShared(){
		return share>1;
	}


	@Override
	public Optional<LabelsImage> origin() {
		return Optional.empty();
	}



	@Override
	public RidgeArea<T> create(float label) {
		RidgeArea<T> n = new RidgeArea<>();
		n.setAttribute(labelKey, label);
		return n;
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return keys.stream();
	}


	@Override
	public long[] location() {
		return getAttribute(Keys.location).get();
	}


	public void location3D(long[] l) {
		long[] l2d = getAttribute(Keys.location).get();
		l[0] = l2d[0]; l[1] = l2d[1];	l[2] = sliceLocation();
	}


	@Override
	public double[] centroid() {
		return getAttribute(Keys.centroid).get();
	}


	public int sliceLocation(){
		return getAttribute(slice).get();
	}


	public long[] bbMin(){
		return getAttribute(Keys.bbMin).get();
	}

	public long[] bbMax(){
		return getAttribute(Keys.bbMax).get();
	}


	public double area(){
		return getAttribute(Keys.volume).get();
	}

	/**
	 * @param other
	 * @param labels the 3D RandomAccessibleInterval where the RidgeArea are defined
	 * @return The area (number of pixels) representing the intersection of the 2 given area
	 */
	public Pair<Double,long[]> intersection(RidgeArea<T> other, RandomAccessibleInterval<T> labels) {

		double result = 0;
	

		Interval bb = enclosingInterval();
		Cursor<T> cursor = Views.interval(labels, bb).cursor();
		RandomAccess<T> access = labels.randomAccess(bb);
		
		T myLabel = access.get().createVariable();
		T otherLabel = myLabel.createVariable();

		myLabel.setReal(label().get());
		otherLabel.setReal(other.label().get());
		
		
		long[] pos = new long[3];
		long[] centroid = new long[3];
		long otherS = other.sliceLocation();
		while(cursor.hasNext()){
			T t = cursor.next();
			if(t.equals(myLabel)){
				cursor.localize(pos);
				pos[2] = otherS;
				access.setPosition(pos);
				if(access.get().equals(otherLabel)){
					result++;
					centroid[0]+=pos[0];	centroid[1]+=pos[1];
				}
			}
		}

		if(result!=0){
			centroid[0]/=result;	centroid[1]/=result;
		}
		centroid[2] = other.sliceLocation();

		return new Pair<>(result,centroid);
	}


	
	private Interval enclosingInterval(){
		long[] bbMin = bbMin();
		long[] bbMax = bbMax();
		long[] threedMin = new long[]{bbMin[0],bbMin[1],sliceLocation()};
		long[] threedMax = new long[]{bbMax[0],bbMax[1],sliceLocation()};
		return new FinalInterval(threedMin,threedMax);
	}





	/**
	 * Repaint this {@link RidgeArea} within the given image with the new provided value
	 * 
	 * @param newLabel The value to repaint the area with
	 * @param slice The image where to repaint the area
	 */
	public void repaint(T newLabel, RandomAccessibleInterval<T> slice) {


		float myLabel = label().get();

		T l = newLabel;
		if(isShared()){
			l = newLabel.createVariable();
			l.setReal(l.getMaxValue());
		}

		Cursor<T> cursor = Views.interval(slice,enclosingInterval()).cursor();

		while(cursor.hasNext()){
			cursor.fwd();
			if(cursor.get().getRealFloat()==myLabel){
				cursor.get().set(l);
			}
		}		

		this.setAttribute(newLabelKey, newLabel.getRealFloat());

	}

	@Override
	public String toString(){
		return "RidgeArea : "
				+ "\n Label -> "+label()
				+ "\n Slice -> "+sliceLocation();
	}



	/**
	 * Draws the centroid of this {@link RidgeArea} using the given {@link RandomAccess} and {@link Type} 
	 * @param access
	 * @param center
	 */
	public void showCenter(RandomAccess<T> access, T center) {	
		long[] pos = new long[]{location()[0],location()[1],sliceLocation()}; 
		access.setPosition(pos);
		access.get().set(center);
	}



	/**
	 * Creates and returns a copy of this {@link RidgeArea} instance to a given image slice. All fields are duplicated (except the {@link #sliceLocation()}).
	 * <br><br>
	 * This method also takes care of updating the graph of potential connections: 
	 * <ul>
	 * 		<li>A new {@link Link} with type {@link AreaToVolume#link} is created between this instance and the copy instance.
	 * 		<li>This properties of this new link are computed except for the Jaccard Index which is given as argument
	 * 		<li>Links are created between the copy and other {@link RidgeArea} located in adjacent planes using 
	 * {@link AreaToVolume#connectSets(List, List, ATVOptions, RandomAccessibleInterval)}.
	 * </ul>
	 * @param labels The label image where the copy should be drawn
	 * @param sliceLocation The index of the plane where the copy is to be created
	 * @param label The unique label for the copy
	 * @param potentials A Collection of link
	 * @param options The {@link ATVOptions} which stores the parameters for the current Depth linkage process
	 * @param ji The value of the Jaccard Index to assign to the link between this {@link RidgeArea} and its copy.
	 * @return The copy {@link RidgeArea}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RidgeArea<T> copyToSlice(RandomAccessibleInterval<T> labels, int sliceLocation, T label, Collection<Link> potentials, ATVOptions options, Double ji) {

		// First copy this object
		RidgeArea<T> ra = this.create(label.getRealFloat());

		this.getValidAttributeKeys().filter(k->k!=SegmentedObject.labelKey).forEach(k->ra.setAttribute((AKey)k,this.getAttribute(k).get()));

		ra.setAttribute(slice, sliceLocation);


		// write label on the image

		Cursor<T> thisCursor = Views.interval(labels, this.enclosingInterval()).cursor();
		Cursor<T> raCursor = Views.interval(labels, ra.enclosingInterval()).cursor();

		T myLabel = labels.randomAccess().get().createVariable();
		myLabel.setReal(label().get());

		while(thisCursor.hasNext()){
			thisCursor.fwd();
			raCursor.fwd();
			if(thisCursor.get().equals(myLabel)){
				raCursor.get().set(label);
			}
		}



		// Now add the link to ourselves

		Link cx = null;
		if(sliceLocation() < sliceLocation){ // if we copy downward

			cx = new DataLink(AreaToVolume.link, this, ra, true);	
			/*
			cx.setAttribute(AreaToVolume.vector, 
					new double[]{0,0,sliceLocation()-sliceLocation});
					*/
		}
		else{

			cx = new DataLink(AreaToVolume.link, ra, this, true);	
			/*
			cx.setAttribute(AreaToVolume.vector, 
					new double[]{0,0,sliceLocation - sliceLocation()});
					*/

		}

		cx.setAttribute(Keys.distance,0d);
		cx.setAttribute(AreaToVolume.overlap, 2d);
		cx.setAttribute(AreaToVolume.jaccard, ji); 
		cx.setAttribute(AreaToVolume.overlapCenter, this.location());



		// finally create links to other areas
		List<RidgeArea<T>> p = new ArrayList<>();
		List<RidgeArea<T>> c = new ArrayList<>();

		if(sliceLocation() < sliceLocation){ // if we copy downward
			p.add(ra);
			potentials.forEach(l->c.add((RidgeArea<T>) l.target()));
		}
		else{
			c.add(ra);
			potentials.forEach(l->p.add((RidgeArea<T>) l.source()));
		}

		AreaToVolume.connectSets(p, c, options, labels);


		return ra;
	}



	/**
	 * @param labels The 3 dimensional RandomAccessibleInterval where the outline should be drawn
	 * @param label The label for the outline
	 */
	public void drawOutline(RandomAccessibleInterval<T> labels, T label) {

		final IntervalView<T> view = Views.interval(labels, this.enclosingInterval());
		final RandomAccess<T> access = view.randomAccess();

		final T myLabel = access.get().createVariable();
		myLabel.setReal(label().get());

		Outline.run(
				view,
				myLabel, 
				(p)->{
					access.setPosition(p);
					access.get().set(label);
				});

	}

	
	
	
	/**
	 * Draws the bounding box of this area in the provided RandomAccessibleInterval (3 dimensional)
	 * @param overlay
	 */
	public void drawBoundingBox(RandomAccessibleInterval<T> overlay) {

		final Interval bb = this.enclosingInterval();
		final IntervalView<T> view = Views.interval(overlay, bb);
		final RandomAccess<T> access = view.randomAccess();

		final T myLabel = access.get().createVariable();
		myLabel.setReal(label().get());

		Cursor<T> cursor = view.localizingCursor();
		long[] pos = new long[3];
		pos[2] = this.sliceLocation();
		while(cursor.hasNext()){
			cursor.fwd();
			cursor.localize(pos);
			if(pos[0] == bb.min(0) || pos[0] == bb.max(0)
					|| pos[1] == bb.min(1) || pos[1] == bb.max(1))
				cursor.get().set(myLabel);
		}
		

	}
	
	
	
	
	/**
	 * @return The volume in pixel of all the {@link RidgeArea} located above this instance
	 */
	@SuppressWarnings("unchecked")
	public double getUpStreamArea() {

		DoubleAdder ua = new DoubleAdder();

		Traversers.breadthfirst(
				this,
				Traversers.newConstraints().fromMinDepth().toMaxDepth()
				.traverseLink(AreaToVolume.link, Direction.INCOMING).includeAllNodes())
		.traverse().nodes
		.forEach(n->ua.add(((RidgeArea<T>) n).area()));

		return ua.doubleValue();
	}

	
	/**
	 * @return The volume in pixel of all the {@link RidgeArea} located below this instance
	 */
	@SuppressWarnings("unchecked")
	public double getDownStreamArea() {

		DoubleAdder ua = new DoubleAdder();

		Traversers.breadthfirst(
				this,
				Traversers.newConstraints().fromMinDepth().toMaxDepth()
				.traverseLink(AreaToVolume.link, Direction.OUTGOING).includeAllNodes())
		.traverse().nodes
		.forEach(n->ua.add(((RidgeArea<T>) n).area()));

		return ua.doubleValue();

	}









}
