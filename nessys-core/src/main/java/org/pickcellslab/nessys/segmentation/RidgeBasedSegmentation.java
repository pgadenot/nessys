package org.pickcellslab.nessys.segmentation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.ProgressControl;
import org.pickcellslab.foundationj.services.ProgressControlListener;
import org.pickcellslab.foundationj.services.ProgressPanel;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ExtendedImageInfo;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.PipelineBuilder;
import org.pickcellslab.pickcells.api.img.pipeline.ProcessingPipeline;
import org.pickcellslab.pickcells.api.img.pipeline.Skip;
import org.pickcellslab.pickcells.api.img.process.AnisotropicDiffusion;
import org.pickcellslab.pickcells.api.img.process.ChannelExtractor;
import org.pickcellslab.pickcells.api.img.process.GaussianFilter;
import org.pickcellslab.pickcells.api.img.steerable.SteerableFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.Type;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;


/**
 * This class is responsible for building and running the Nessys segmentation pipeline
 *
 */
public class RidgeBasedSegmentation{


	private final ImgIO io;
	private final NotificationFactory notifier;
	private final UITheme theme;
	
	private static final Logger log = LoggerFactory.getLogger(RidgeBasedSegmentation.class);

	/**
	 * Constructs a new {@link RidgeBasedSegmentation} using the given inputs as context
	 * @param io An instance of {@link ImgIO} used for image I/O operations
	 * @param notifier An instance of {@link NotificationFactory} used for displaying notifications to the user
	 * @param theme The {@link UITheme} to be used in the GUI.
	 */
	public RidgeBasedSegmentation(ImgIO io, NotificationFactory notifier, UITheme theme) {
		this.io = io;
		this.notifier = notifier;
		this.theme = theme;
	}



	/**
	 * Creates and runs the pipeline
	 * @param <T> The {@link Type} of the input image ({@link RandomAccessibleInterval})
	 */
	public <T extends RealType<T> & NativeType<T>> void run(){


		// Create The Pipeline

		// Denoising
		final List<ImgProcessing<T,FloatType>> denoising = new ArrayList<>();
		denoising.add(new GaussianFilter<>(io, notifier));
		denoising.add(new AnisotropicDiffusion<>(io));
		denoising.add(new Skip<>(io, new FloatType(), DimensionalityEffect.FLAT_TO_FLAT));

		// Ridge enhancement
		final List<ImgProcessing<FloatType,FloatType>> ridges = new ArrayList<>();	
		ridges.add(new SteerableFilter<>(notifier, io));
		ridges.add(new Skip<>(io, new FloatType(), DimensionalityEffect.FLAT_TO_FLAT));


		RidgeSegmentationPipe<FloatType> rs = new RidgeSegmentationPipe<>(notifier, io);
		AreaToVolume<FloatType> atv = new AreaToVolume<>(notifier, io, rs.getOptions());


		Optional<ProcessingPipeline<T,FloatType>> pipeline =	
				new PipelineBuilder<T,T>()
				.step(new ChannelExtractor<T>(), "Extract One Channel")
				.steps(denoising, "Denoising...")
				.steps(ridges, "Ridge Enhancement...")
				.step(rs, "Slice by Slice Segmentation")
				.step(atv, "Merge Areas to 3D Volumes")
				.buildForUserChosenFiles(theme, notifier, io, io.createCheckerBuilder()
						.addCheck(ImgsChecker.calibration)
						.addCheck(ImgsChecker.ordering));



		if(pipeline.isPresent()){

			
			// Create the UI and Control
			final ProgressPanel<ExtendedImageInfo<FloatType>> progressUI = pipeline.get().runPipeline(io, (n)->n+"_Segmentation");
			final ProgressControl<ExtendedImageInfo<FloatType>> control = progressUI.getControl();
			
			
			// Register a listener to notify the user when all images have been processed			
			control.addProgressListener(new ProgressControlListener(){

				@Override
				public void progressStarted() {}

				@Override
				public void progressCompleted() {
					notifier.notify(null, "Pipeline completed - All images saved!", 2000, icon());
				}

				@Override
				public void progressAborted() {}
				
			});
			
			progressUI.setTitle("Nessys Progress");
			progressUI.setVisible(true);

		}


	}


	public static String name() {
		return "NESSYs";
	}


	public static Icon icon() {
		return new ImageIcon(RidgeBasedSegmentation.class.getResource("/icons/RBS_Icon_32.png"));
	}


	public static String description() {
		return "<html><b>NESSYs: Nuclear Envelope Segmentation SYstem</b>"
				+ "<br> Automatically identifies cell nuclei in biological images (2D - 3D - 3D + time).</HTML>";
	}

}
