package org.pickcellslab.nessys.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedPrototype;

import net.imglib2.Cursor;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.IntervalView;

/**
 * Provides static helper method useful for working with segmented images
 *
 */
public final class SegmentationUtil {

	
	/**
	 * 
	 * Creates a {@link SegmentedPrototype} for each label found within a given a labeled image and returns a Map where 
	 * the created {@link SegmentedPrototype} (values) are mapped to their label (keys).
	 * 
	 * @param view The labeled image for which to create {@link SegmentedPrototype}s
	 * @param p A {@link SegmentedPrototype} to use as factory for creating other {@link SegmentedPrototype}s
	 * @return A Map where the created {@link SegmentedPrototype} (values) are mapped to their label (keys). Note that all 
	 * {@link SegmentedPrototype} will hold basic features including, volume, location, centroid, bounding box coordinates, and second
	 * moments of the object shape.
	 */
	public static <T extends RealType<T>,P extends SegmentedPrototype> Map<T,P> createMap(IntervalView<T> view, P p){
		


		HashMap<T,SummaryStatistics[]> statsMap = new HashMap<>();
		ConcurrentHashMap<T,P> dataMap = new ConcurrentHashMap<>();
		
		T zero = view.firstElement().createVariable();

		Cursor<T> c = view.cursor();
		while(c.hasNext()){
			
			T l = c.next();
			if(!l.equals(zero)){
				l = l.copy();
				SummaryStatistics[] stats = statsMap.get(l);
				if(stats == null){// if new label
					//Creat stats
					stats = new SummaryStatistics[c.numDimensions()];
					for(int i = 0; i<c.numDimensions(); i++)
						stats[i] = new SummaryStatistics();

					statsMap.put(l, stats);
					//Create the segmented object which will store the stats
					@SuppressWarnings("unchecked")
					P newP = (P) p.create(l.getRealFloat());
					dataMap.put(l, newP);						
				}
				//Add value to stats
				for(int i = 0; i<c.numDimensions(); i++)				
					stats[i].addValue(c.getDoublePosition(i));				
			}
			//else
				//c.get().setReal(65000);
		}

		//Store stats to the dataMap objects
		statsMap.entrySet().stream().parallel().forEach(e->{

			SegmentedObject item = dataMap.get(e.getKey());
			double[] center = new double[c.numDimensions()];
			double[] moments = new double[c.numDimensions()];
			long[] loc = new long[c.numDimensions()];
			long[] min = new long[c.numDimensions()];
			long[] max = new long[c.numDimensions()];

			for(int i = 0; i<c.numDimensions(); i++){
				center[i] = e.getValue()[i].getMean();
				moments[i] = e.getValue()[i].getSecondMoment();
				loc[i] = (long) e.getValue()[i].getMean();
				min[i] = (long) e.getValue()[i].getMin();
				max[i] = (long) e.getValue()[i].getMax();
			}

			//perform calibration where necessary
			item.setAttribute(Keys.volume, ((double)e.getValue()[0].getN()));
			item.setAttribute(Keys.location, loc);
			item.setAttribute(Keys.centroid, center);
			item.setAttribute(Keys.second, moments);
			item.setAttribute(Keys.bbMin, min);
			item.setAttribute(Keys.bbMax, max);

		});

		statsMap = null;
		
		return dataMap;
		
	}
	
	
	
}
