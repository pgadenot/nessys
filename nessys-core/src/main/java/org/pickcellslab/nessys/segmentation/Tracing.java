package org.pickcellslab.nessys.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;

import net.imglib2.RandomAccess;
import net.imglib2.type.numeric.RealType;

/**
 * Provides static helper methods for the ridge tracing process 
 *
 */
public final class Tracing {





	/**
	 * Tracing kernel ([position][xy coordinates])
	 */
	private static final long[][] p = {

			{0,1},
			{1,1},
			{1,0},
			{1,-1},
			{0,-1},
			{-1,-1},
			{-1,0},
			{-1,1}			

	};



	/**
	 * Stores the location of the possible pixels to be evaluated for inclusion in the trace ("front of migration").
	 * The locations are stored as indices in {@link #p}
	 * For example if the rear of the trace is in the top left (7 in {@link #p}) :
	 * <pre>
	 *    n[7] = {p[2],p[3],p[4]}, which corresponds to:
	 *    
	 *    R x x
	 *    x C v
	 *    x v v
	 *    
	 *    R: Rear, C: Current position, x: illegal, p: potential
	 * </pre> 
	 */
	private static long[][][] fronts = {

			{p[3],p[4],p[5]},
			{p[4],p[5],p[6]},
			{p[5],p[6],p[7]},
			{p[6],p[7],p[0]},
			{p[7],p[0],p[1]},
			{p[0],p[1],p[2]},
			{p[1],p[2],p[3]},
			{p[2],p[3],p[4]}			

	};







	private Tracing(){}




	
	
	public static int getCode(long[] current, long[] next, long[] temp){
		temp[0] = next[0]-current[0];	temp[1] = next[1]-current[1];
		for(int f = 0; f<p.length; f++){
			if(Arrays.equals(p[f], temp))
				return f;
			}		
		throw new IllegalArgumentException("Illegal locations, current and next are not neighbour pixels -> "+Arrays.toString(temp));
	}


	public static List<Integer> toChainCode(List<long[]> track) {
		
		if(track.size()<2)
			return Collections.emptyList();
		
		List<Integer> cc = new ArrayList<>();
		long[] current = track.get(0);
		long[] temp = new long[2];
		for(int i = 1; i<track.size(); i++){
			cc.add(Tracing.getCode(current, track.get(i), temp));
			current = track.get(i);
		}
		return cc;
	}




	/**
	 * @param access An access to the track image
	 * @param center The current location of the migrating track
	 * @param temp A temporary long[] used for intermediate operations (to avoid instantiating one evry time)
	 * @param id The id of the migrating track
	 * @return The front of migration as a list of locations 
	 */
	public static <T> long[][] getFront(RandomAccess<T> access, long[] center, long[] temp, T id){
		for(int f = 0; f<p.length; f++){
			temp[0] = p[f][0] + center[0];			temp[1] = p[f][1] + center[1];
			access.setPosition(temp);
			if(access.get().equals(id)){
				//System.out.println("index :" +f);
				return fronts[f];
			}
		}
		System.out.println("Tracing -- getFront --> Returning full!");
		return p;
	}


	
	
	/**
	 * @param source The location where a neighbour pixel is to be found
	 * @param target The target location
	 * @return The location of the neighbour pixel to source lying on the opposite side of target 
	 */
	public static long[] getLure(long[] source, long[] target){

		long[] lure = Arrays.copyOf(source, source.length);
		long[] offset = null;

		double atan =  Math.atan2(source[0] - target[0], source[1] - target[1])/(Math.PI/4);
		//System.out.println(Math.toDegrees(Math.atan2(source[1]-target[1] ,source[0]-target[0])));

		if(atan>=0)
			offset = p[(int) Math.round(atan)];
		else {
			int i = (int) Math.round(atan);
			if(i == 0)
				offset = p[0];
			else offset = p[8+i];
		}

		lure[0]+=offset[0];
		lure[1]+=offset[1];
		return lure;
	}






	public static <T extends RealType<T>, V extends RealType<V>> List<Ridge<T,V>> getMostProbableTracks(RidgePoint<T,V> root,  Map<Float,Ridge<T,V>> neighbours, RidgeSegmentationOptions o){


		Objects.requireNonNull(root,"root is null");

		
		// Get all leaves for each child or root
		List<RidgePoint<T,V>> leaves = getLeaves(root);
	
		//System.out.println("Number of leaves : "+leaves.size());
		
		// Create all possible paths (as polyline)
		List<Ridge<T,V>> ridges = new ArrayList<>();

		//Combinatoric loops
		for(int i = 0; i<leaves.size(); i++)
			for(int j = i+1; j<leaves.size(); j++)
				ridges.add(new Ridge<>(root.intensity, leaves.get(i),leaves.get(j)));

		ridges = ridges.stream().parallel().sorted((r1,r2)->Double.compare(r2.score(o, neighbours), r1.score(o, neighbours))).collect(Collectors.toList());
	//	Collections.sort(ridges,(r1,r2)->Double.compare(r2.score(o), r1.score(o)));
		//System.out.println("Number of leaves --> "+leaves.size());
		//System.out.println("Number of ridges --> "+ridges.size());

		return ridges;

	}





	private static <T extends RealType<T>, V extends RealType<V>> List<RidgePoint<T,V>> getLeaves(RidgePoint<T,V> rp) {
		List<RidgePoint<T,V>> leaves = new ArrayList<>();
		Deque<RidgePoint<T,V>> queue = new LinkedList<>();
		queue.add(rp);
		while(!queue.isEmpty()){
			RidgePoint<T,V> node = queue.poll();
			if(node.numChildren() == 0 )//&& node.isOn())
				leaves.add(node);
			else
				for(int c = 0 ; c<node.numChildren(); c++){
					if(node.get(c).isOn()) 
						queue.add(node.get(c));
				}
		}
		return leaves;
	}





	public static Pair<Integer,Double> closestPoint(long[] childCenter, List<long[]> m) {
		
		int index = -1;
		double closest = Double.MAX_VALUE;
		for(int i = 0; i<m.size(); i++){
			double dist = ImgGeometry.distance(m.get(i), childCenter);
			if(dist<closest){
				closest = dist;
				index = i;
			}
		}
		
		return new Pair<Integer,Double>(index,closest);
	}









	public static Pair<Integer, Double> farthestPoint(long[] start, List<long[]> track) {
		
		int index = -1;
		double closest = -1;
		for(int i = 0; i<track.size(); i++){
			double dist = ImgGeometry.distance(track.get(i), start);
			if(dist>closest){
				closest = dist;
				index = i;
			}
		}
		
		return new Pair<Integer,Double>(index,closest);
	}


	
	
	public static void main(String[] args) {

		long[][] circle = ImgGeometry.clockwiseKernel(20);
		long[] current = circle[0];
		long[] temp = new long[2];
		for(int i = 1; i<circle.length; i++){
			System.out.print(Tracing.getCode(current, circle[i], temp));
			current = circle[i];
		}

		// And last point
		System.out.print(Tracing.getCode(current, circle[0], temp));
		System.out.println("");
		Tracing.toChainCode(Arrays.asList(circle)).forEach(l->System.out.print(l));
		
	}






	
	
	


}
