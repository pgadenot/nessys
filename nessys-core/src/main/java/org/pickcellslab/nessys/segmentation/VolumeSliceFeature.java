package org.pickcellslab.nessys.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.imglib2.RandomAccess;
import net.imglib2.type.numeric.RealType;

/**
 * A {@link VolumeSliceFeature} is responsible for computing measures (features) for a specific {@link RidgeArea} within a {@link Volume}. 
 *
 */
public interface VolumeSliceFeature {

	/**
	 * Visits a position during the iteration over the pixels that compose a {@link RidgeArea}.
	 * @param labels A {@link RandomAccess} on the labels image (which contains the segmented shapes) 
	 * @param grays A {@link RandomAccess} on the original image
	 * @param pos The coordinates of the current pixel in the iteration (NB: you should not modify the values held by this array)
	 * @param temp a long[] with same dimensionality as pos which can be used to position {@link RandomAccess} at other positions than the current one.
	 * (provided here to avoid creating an array for each call)
	 * @param paint The {@link RealType} of the labels and grays image.
	 */
	<T extends RealType<T>> void accept(RandomAccess<T> labels, RandomAccess<T> grays, long[] pos, long[] temp, T paint);

	/**
	 * Notifies this {@link VolumeSliceFeature} that all pixels of the {@link RidgeArea} have been iterated through and gives
	 * a chance to finalise the result.
	 * 
	 * @param grays
	 * @param temp
	 * @return
	 */
	<T extends RealType<T>> boolean doneWithPositions(RandomAccess<T> grays, long[] temp);

	/**
	 * @return the area (in number of pixels) of the {@link RidgeArea} this {@link VolumeSliceFeature} has computed features for.
	 */
	int area();

	/**
	 * @return the centroid of the {@link RidgeArea} this {@link VolumeSliceFeature} has computed features for.
	 */
	long[] centroid();
}
