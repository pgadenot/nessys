package org.pickcellslab.nessys.plug.pickcells;

/*-
 * #%L
 * Nessys
 * %%
 * Copyright (C) 2016 - 2018 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.JOptionPane;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.nessys.shared.ImgInputProvider;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.RawFile;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;

/**
 * {@link ImgInputProvider} implementation which offers the user to choose from objects stored in the database
 *
 */
public class DBImgInputProvider implements ImgInputProvider {

	private final DataAccess access;

	public DBImgInputProvider(DataAccess access) {
		Objects.requireNonNull(access);
		this.access = access;
	}


	@Override
	public Image getOneImage(String msg, Icon icon) {

		try {

			RegeneratedItems items = 
					access.queryFactory().regenerate(DataRegistry.typeIdFor(SegmentationResult.class))
					.toDepth(2)
					.traverseLink(Links.COMPUTED_FROM, Direction.OUTGOING)
					.and(Image.RAW_FILE_LINK,  Direction.OUTGOING)
					.includeAllNodes().regenerateAllKeys().doNotGetAll()
					.useFilter(F.streamKeys().contains(SegmentationResult.associated).negate())
					.run();


			if(items.getAllTargets().isEmpty()){
				JOptionPane.showMessageDialog(null, "There are no editable Segmentation in the database");
				return null;
			}


			List<RawFile> rt = items.getAllItemsFor(RawFile.class).collect(Collectors.toList());

			RawFile raw = (RawFile) JOptionPane.showInputDialog(
					null,
					"Choose the image to be edited",
					"Segmentation Editor...",
					JOptionPane.PLAIN_MESSAGE,
					icon,
					rt.toArray(),
					rt.get(0)
					);

			if(null == raw)
				return null;

			return raw.getImage();
		}
		catch(DataAccessException e) {
			return null;
		}
	}

	@Override
	public LabelsImage getOneLabels(Image image, String msg, Icon icon) {


		List<SegmentationResult> segs = image.segmentations();		// no need to filter for no associations, this was done when regenerating from the database	
		SegmentationResult sr = segs.get(0);

		if(segs.size()>1){
			sr = (SegmentationResult) JOptionPane.showInputDialog(
					null,
					"Several results are associated with this image, choose the one to edit",
					"Segmentation Editor...",
					JOptionPane.PLAIN_MESSAGE,
					icon,
					segs.toArray(),
					segs.get(0)
					);
		}
		return sr;
	}
	
}
