package org.pickcellslab.nessys.plug.pickcells;

/*-
 * #%L
 * Nessys
 * %%
 * Copyright (C) 2016 - 2018 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.ProgressControl;
import org.pickcellslab.foundationj.services.ProgressControlListener;
import org.pickcellslab.foundationj.services.ProgressPanel;
import org.pickcellslab.foundationj.services.TaskProgress.ProgressStatus;
import org.pickcellslab.foundationj.services.TaskResultConsumer;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.nessys.segmentation.AreaToVolume;
import org.pickcellslab.nessys.segmentation.RidgeBasedSegmentation;
import org.pickcellslab.nessys.segmentation.RidgeSegmentationPipe;
import org.pickcellslab.pickcells.api.app.data.Experiment;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ExtendedImageInfo;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.ProcessingPipeline;
import org.pickcellslab.pickcells.api.img.pipeline.Skip;
import org.pickcellslab.pickcells.api.img.process.AnisotropicDiffusion;
import org.pickcellslab.pickcells.api.img.process.ChannelExtractor;
import org.pickcellslab.pickcells.api.img.process.GaussianFilter;
import org.pickcellslab.pickcells.api.img.steerable.SteerableFilter;
import org.pickcellslab.pickcells.api.imgdb.pipeline.PipelineBuilderForDBImages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;

@Module
public class RidgeBasedSegmentationAdapter extends AbstractAnalysis{

	private static final Logger log = LoggerFactory.getLogger(RidgeBasedSegmentationAdapter.class);

	private boolean isActive = false;

	private final DataAccess access;
	private final ImgIO io;
	private final NotificationFactory notifier;
	private final UITheme theme;

	private File home;
	private String resultsName = "Segmentation";

	public RidgeBasedSegmentationAdapter(DataAccess access, ImgIO io, NotificationFactory notifier, UITheme theme) {
		this.access = access;
		this.io = io;
		this.notifier = notifier;
		this.theme = theme;
	}



	@Override
	public Icon[] icons() {
		return new Icon[1];
	}

	@Override
	public String[] steps() {
		return new String[]{""};
	}

	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/RBS_Icon_32.png"));
	}

	@Override
	public void launch() throws AnalysisException {
		try {
			isActive = true;

			setStep(0);

			//First get the image data
			RegeneratedItems set;
			try {
				if(home == null){
					//Get the image folder
					String f = access.queryFactory().read("Experiment").makeList(Experiment.dbPathKey).inOneSet().getAll().run().get(0);
					home = new File(f);		
				}

				set = access.queryFactory().regenerate(Image.class)
						.toDepth(2)
						.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
						.includeAllNodes()
						.regenerateAllKeys()
						.getAll();

			} catch ( Exception e1) {
				throw new AnalysisException("Unable to read the database", e1);
			}

			final List<Image> images = set.getTargets(Image.class).collect(Collectors.toList());		
			//Sort the list to maintain the same indexing as original images
			Collections.sort(images, (i1,i2)->i1.getAttribute(Keys.name).get().compareTo(i2.getAttribute(Keys.name).get()));

			if(images.isEmpty()){
				log.warn("No images were found in the database");
				return;
			}


			resultsName = getResultName();
			if(resultsName == null){
				resultsName = "NESSys";
				return;
			}

			process();			

			isActive = false;			
		} catch (Exception e) {
			isActive = false;
			throw new AnalysisException("Unable to open the color image", e);
		}
	}



	private <T extends RealType<T> & NativeType<T>> void  process() {

		// Create The Pipeline

		// Denoising
		final List<ImgProcessing<T,FloatType>> denoising = new ArrayList<>();
		denoising.add(new GaussianFilter<>(io, notifier));
		denoising.add(new AnisotropicDiffusion<>(io));
		denoising.add(new Skip<>(io, new FloatType(), DimensionalityEffect.FLAT_TO_FLAT));

		// Ridge enhancement
		final List<ImgProcessing<FloatType,FloatType>> ridges = new ArrayList<>();	
		ridges.add(new SteerableFilter<>(notifier, io));
		ridges.add(new Skip<>(io, new FloatType(), DimensionalityEffect.FLAT_TO_FLAT));


		RidgeSegmentationPipe<FloatType> rs = new RidgeSegmentationPipe<>(notifier, io);
		AreaToVolume<FloatType> atv = new AreaToVolume<>(notifier, io, rs.getOptions());


		Optional<ProcessingPipeline<T,FloatType>> pipeline =	
				new PipelineBuilderForDBImages<T,T>()
				.step(new ChannelExtractor<T>(), "Extract One Channel")
				.steps(denoising, "Denoising...")
				.steps(ridges, "Ridge Enhancement...")
				.step(rs, "Slice by Slice Segmentation")
				.step(atv, "Merge Areas to 3D Volumes")
				.buildForDBImages(theme, notifier, io, access, icon(), "NESSys Segmentation...");



		if(pipeline.isPresent()){

			// Get a ref to progress control
			final ProgressPanel<ExtendedImageInfo<FloatType>> progressUI = pipeline.get().runPipeline(io, (n)->n+"_"+resultsName);
			final ProgressControl<ExtendedImageInfo<FloatType>> control = progressUI.getControl();
			
			
			// Setup a consumer which will save the result to database if the task succeeded
			final TaskResultConsumer<ExtendedImageInfo<FloatType>> save = (task, info)->{

				if(task.getStatus() == ProgressStatus.SUCCESS) {

					new SegmentationResult(info.getAssociatedImage(), 
							info.getName() + io.standard(info.getInfo().imageDimensions(),						
									info.getType().getBitsPerPixel()),
							resultsName,
							3,
							"Computed with NESSys");
					try {
						access.queryFactory().store().add(info.getAssociatedImage()).run();
					} catch (DataAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			};

			

			for(int t = 0; t<control.numTasks(); t++)
				control.getProgress(t).addTaskResultConsumer(save);
			
			
			// TODO: we could also setup a listener to interrupt the batch process in case an error occurs for one image
			// and roll back for stored SegmentationResults
			/*
			control.addProgressExceptionHandler(new ProgressExceptionHandler<ExtendedImageInfo<FloatType>>() {

				@Override
				public void handle(ProgressControl<ExtendedImageInfo<FloatType>> source,
						TaskProgress<ExtendedImageInfo<FloatType>> task, CancellationException e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void handle(ProgressControl<ExtendedImageInfo<FloatType>> source,
						TaskProgress<ExtendedImageInfo<FloatType>> task, InterruptedException e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void handle(ProgressControl<ExtendedImageInfo<FloatType>> source,
						TaskProgress<ExtendedImageInfo<FloatType>> task, ExecutionException e) {
					// TODO Auto-generated method stub
					
				}
				
			});
			*/
			

			// Add a listener to notify the user about the fact that images were saved
			control.addProgressListener(new ProgressControlListener() {

				@Override
				public void progressStarted() {}

				@Override
				public void progressCompleted() {
					notifier.notify(null, "Pipeline successful - Image saved!", 2000, icon());
				}

				@Override
				public void progressAborted() {}
				
			});
			
			progressUI.setTitle("Nessys Progress");
			progressUI.setVisible(true);

		}


	}



	private String getResultName() {
		String name = JOptionPane.showInputDialog(null, "Choose a name for the segmentation you will generate",resultsName);
		if(name == ""){
			JOptionPane.showMessageDialog(null, "Please enter a valid name");
			return getResultName();
		}
		return name;
	}



	@Override
	public String name() {
		return RidgeBasedSegmentation.name();
	}





	@Override
	public boolean isActive() {
		return isActive;
	}





	@Override
	public String[] categories() {
		return new String[]{"Segmentation"};
	}

	@Override
	public String description() {
		return RidgeBasedSegmentation.description();
	}





	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}





	@Override
	public void start() {
		// TODO Auto-generated method stub

	}





	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public URL url() {
		try {
			return new URL("https://framagit.org/pickcellslab/nessys");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	}







}
