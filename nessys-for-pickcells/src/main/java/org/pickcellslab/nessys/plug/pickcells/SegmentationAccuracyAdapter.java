package org.pickcellslab.nessys.plug.pickcells;

/*-
 * #%L
 * Nessys
 * %%
 * Copyright (C) 2016 - 2018 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.net.URL;

import javax.swing.Icon;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.nessys.accuracy.SegmentationAccuracy;
import org.pickcellslab.pickcells.api.app.modules.DesktopModule;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;

import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;


@Module
public class SegmentationAccuracyAdapter<R extends NativeType<R> & RealType<R>, T extends NativeType<T> & RealType<T>> implements DesktopModule{

	private final SegmentationAccuracy<R,T> core;
		
	
	public SegmentationAccuracyAdapter(ProviderFactoryFactory pff, ImgIO io) {
		core = new SegmentationAccuracy<>(pff, io);
	}


	@Override
	public void launch() throws Exception {
		core.run();
	}


	@Override
	public String name() {
		return core.name();
	}

	@Override
	public String description() {
		return core.description();
	}


	@Override
	public Icon icon() {
		return core.icon();
	}



	@Override
	public String authors() {
		return "Guillaume Blin";
	}


	@Override
	public String licence() {
		return "GPLv3";
	}


	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}

}
