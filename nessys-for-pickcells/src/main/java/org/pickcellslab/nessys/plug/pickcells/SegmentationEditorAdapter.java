package org.pickcellslab.nessys.plug.pickcells;

/*-
 * #%L
 * Nessys
 * %%
 * Copyright (C) 2016 - 2018 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.nessys.editing.SegmentationEditor;
import org.pickcellslab.nessys.shared.ImgInputProvider;
import org.pickcellslab.pickcells.api.app.modules.ActivationListener;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.app.modules.Task;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.view.ImageDisplayFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;

@Module
public class SegmentationEditorAdapter<S extends RealType<S> & NativeType<S>, T extends RealType<T> & NativeType<T>> implements Task, MetaModelListener{


	private final DataAccess access;
	private final SegmentationEditor<S,T> core;

	private static final Logger log = LoggerFactory.getLogger(SegmentationEditorAdapter.class);

	private final List<ActivationListener> alLstrs = new ArrayList<>();

	private boolean isActive = false;



	public SegmentationEditorAdapter(DataAccess access, ProviderFactoryFactory pff, ImgIO io, ImageDisplayFactory dispFctry, UITheme theme, NotificationFactory notifier){
		
		final ImgInputProvider inputProvider = new DBImgInputProvider(access);
		core = new SegmentationEditor<>(inputProvider, pff, io, dispFctry, theme, notifier);
		this.access = access;
		
	}


	@Override
	public void launch() throws AnalysisException {
		
		try {
			isActive = true;
			core.run();
			isActive = false;			
		} catch (Exception e) {
			isActive = false;
			throw new AnalysisException("Unable to open the color image", e);
		}

	}




	@Override
	public Icon icon() {
		return core.icon();
	}



	@Override
	public String[] categories() {
		return new String[]{"Editing"};
	}

	@Override
	public String name() {
		return core.name();
	}

	@Override
	public String description() {
		return core.description();
	}





	@Override
	public void registerListener(ActivationListener lst) {
		alLstrs.add(lst);
	}



	@Override
	public boolean isActive() {
		return isActive;
	}




	@Override
	public void metaEvent(RegeneratedItems meta, MetaChange evt) {
		testActivability();
	}


	private void testActivability() {
		try {

			boolean wasActive = isActive;

			isActive = 
					! access.queryFactory().read(DataRegistry.typeIdFor(SegmentationResult.class)).makeList(DataItem.idKey)
					.inOneSet().useFilter(F.streamKeys().contains(SegmentationResult.associated)).run().isEmpty();

			if(isActive != wasActive)
				alLstrs.forEach(l->l.isNowActive(this, isActive));

		} catch (DataAccessException e) {
			isActive = false;
			alLstrs.forEach(l->l.isNowActive(this, isActive));
			log.warn("Segmentation editor failed to read the database -> set to inactive ", e);
		}
	}




	@Override
	public void start() {
		testActivability();
	}




	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}

}
