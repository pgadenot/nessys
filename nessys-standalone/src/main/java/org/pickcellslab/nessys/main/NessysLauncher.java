package org.pickcellslab.nessys.main;

/*-
 * #%L
 * Nessys
 * %%
 * Copyright (C) 2016 - 2018 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.jfree.simple.JFSimpleHistogramFactory;
import org.pickcellslab.nessys.accuracy.SegmentationAccuracy;
import org.pickcellslab.nessys.editing.SegmentationEditor;
import org.pickcellslab.nessys.segmentation.RidgeBasedSegmentation;
import org.pickcellslab.nessys.shared.DefaultImgInputProvider;
import org.pickcellslab.nessys.shared.ImgInputProvider;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.view.ImageDiplayFactoryImpl;
import org.pickcellslab.pickcells.api.img.view.ImageDisplayFactory;
import org.pickcellslab.pickcells.impl.notifications.NotificationFactoryImpl;
import org.pickcellslab.pickcells.impl.providers.PickCellsPFFactory;
import org.pickcellslab.pickcells.impl.theme.UIThemeImpl;
import org.pickcellslab.pickcells.ioImpl.PickCellsImgIO;
import org.slf4j.LoggerFactory;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.WebLookAndFeel;
import com.alee.managers.WebLafManagers;

public class NessysLauncher {

	// UI related modules
	private final UITheme theme = new UIThemeImpl();
	private final NotificationFactory notifier = new NotificationFactoryImpl();

	// Image related modules
	private final ImgIO io = new PickCellsImgIO();
	private final ImageDisplayFactory dispFctry = new ImageDiplayFactoryImpl(new JFSimpleHistogramFactory(), theme, notifier);
	private final ProviderFactoryFactory pff = new PickCellsPFFactory(io, notifier);
	private final ImgInputProvider inputProvider = new DefaultImgInputProvider(io);


	public NessysLauncher() {

		final RidgeBasedSegmentation segmenter = new RidgeBasedSegmentation(io, notifier, theme);
		final SegmentationEditor<?,?> editor = new SegmentationEditor<>(inputProvider, pff, io, dispFctry, theme, notifier);
		final SegmentationAccuracy<?,?> accuracy = new SegmentationAccuracy<>(pff, io);


		// Create the launcher dialog

		// Segmenter Button
		final JButton segBtn = new JButton(UITheme.resize(segmenter.icon(), 64, 64));
		notifier.setTooltip(segBtn, "Launch the automated segmentation tool");
		segBtn.addActionListener(a-> segmenter.run());

		// Editor Button
		final JButton edBtn = new JButton(UITheme.resize(editor.icon(), 64, 64));
		notifier.setTooltip(edBtn, "Launch the segmentation editor");
		edBtn.addActionListener(a-> editor.run());

		// Accuracy Button
		final JButton accBtn = new JButton(UITheme.resize(accuracy.icon(), 64, 64));
		notifier.setTooltip(accBtn, "Launch a segmentation accuracy test");
		accBtn.addActionListener(a-> {
			try {
				accuracy.run();
			} catch (Exception e) {
				notifier.display("Error", "Something went wrong with the accuracy tool - see below. "
						+ "The software will shut down", e, Level.SEVERE);
				System.exit(1);
			}
		});


		final JFrame frame = new JFrame("NESSys");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setIconImage(UITheme.toImage(segmenter.icon()));
		
		final JPanel pane = new JPanel();
		pane.setLayout(new VerticalFlowLayout());
		final JLabel label = new JLabel("Select the tool you would like to use");
		label.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		pane.add(label);
		final GroupPanel gp = new GroupPanel(5, segBtn, edBtn, accBtn);
		gp.setBorder(BorderFactory.createEmptyBorder(5,5,10,5));
		pane.add(gp);
		
		frame.setContentPane(pane);		
		frame.setLocationRelativeTo(null);
		frame.pack();		
		frame.setVisible(true);
		
				
	}



	public static void main(String[] args) {

		// Init Look&Feel
		WebLookAndFeel.install();
		WebLafManagers.initialize ();

		try {
			
			new NessysLauncher();
			
		}catch(Exception e) {
			LoggerFactory.getLogger(NessysLauncher.class).error("Unexpected error", e);
			System.exit(1);
		}

	}

}
