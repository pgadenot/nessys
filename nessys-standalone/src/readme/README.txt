#-------------------------------------------------#
#   Nessys: Nuclear Envelope Segmentation System  #
#-------------------------------------------------#

Nessys is is an extensible software written in Java for the automated identification of cell nuclei in biological images (3D + time).
It is designed to perform well in complex samples, i.e when cells are particularly crowded and heterogeneous such as in embryos or in 3D cell cultures.
Nessys is also fast and will work on large images which do not fit in memory.

More info is available on our GitLab repository:
https://framagit.org/pickcellslab/nessys


#------------------------------#
#   Installation instructions  #
#------------------------------#

Nessys requires Java 8 or later. It is recommended to use a 64bit version of java to allow Nessys to use more than 2GB of RAM.

To test the version of java installed on your computer, open a terminal and type:

java -version

You should see something like this:

java version "1.8.0_171"
Java(TM) SE Runtime Environment (build 1.8.0_171-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)

If no java is detected or if an older version is installed, please follow instructions to install java 8 on the Oracle or OpenJDK websites:

OpenJDK: http://openjdk.java.net/
Oracle: http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html

Once java is installed, simply unzip the Nessys archive in a writable folder of your choice on your computer.


#------------------------------#
#     Running instructions     #
#------------------------------#


Windows:
--------

Double click on 'startup.bat'


Linux or Mac:
-------------

Open a terminal

cd to the folder where you extracted Nessys and type:

chmod +x startup.sh
./startup.sh


#------------------------------#
#       Troubleshooting        #
#------------------------------#


If you get an error such as:

java -Xms1024m -Xmx4g -cp "bin/*;lib/*" org.pickcellslab.nessys.NessysLauncher 
Invalid maximum heap size: -Xmx4g
The specified size exceeds the maximum representable size.
Error: Could not create the Java Virtual Machine.
Error: A fatal exception has occurred. Program will exit.


This error may indicate that you are running a 32 bit version of java. If your operating system is 64 bit, please upgrade to 64 bit java.

If you are running a 32 bit operating system, then the the maximum amount of memory you can allocate is 2GB.

To update the maximum memory allocated:

    Open the startup script in a text editor:
        Unix users: open startup.sh.
        Windows users: open startup.bat.
    Edit the -Xmx4g setting to specify a new memory allocation:
        4g means 4GB.
        To reduce this to 2GB, update the setting to -Xmx2g.
    Save your changes.










