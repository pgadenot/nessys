#!/bin/bash
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export CLASSPATH=$CLASSPATH:"$dir"/bin/*:"$dir"/lib/*"
echo $CLASSPATH
java -Xms1024m -Xmx4096m  org.pickcellslab.nessys.main.NessysLauncher
